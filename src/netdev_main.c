/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>

#include "netdev_netlink.h"
#include "netdev_util.h"

#include "dm_netdev.h"
#include "dm_link.h"
#include <dm_link_addr.h>
#include <dm_link_route.h>
#include "dm_link_neigh.h"
#include "netdev_bridgetable.h"
#include "netdev_rpc_utils.h"

#define ME "netdev"

typedef struct {
    amxd_dm_t* dm;                  // the netdev data model object
    amxo_parser_t* parser;          // the odl parser
    int nlfd;                       // netlink socket
} netdev_t;

static netdev_t netdev;

static void rtnlhandler(int fd, void UNUSED* priv) {
    SAH_TRACEZ_IN(ME);
    rtnl_read(fd, rtnlmsg_instance(), false);
    SAH_TRACEZ_OUT(ME);
}

static int netdev_init(amxd_dm_t* dm,
                       amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);

    bool trace = false;
    amxd_object_t* netdev_root = NULL;
    int retval = -1;

    netdev.dm = dm;
    netdev.parser = parser;
    netdev.nlfd = rtnl_open();

    if(netdev.nlfd < 0) {
        goto exit;
    }

    retval = amxo_connection_add(parser, netdev.nlfd, rtnlhandler, NULL, AMXO_CUSTOM, NULL);
    when_failed_trace(retval, exit, ERROR, "amxo_connection_add() failed");

    retval = amxo_parser_scan_mib_dir(parser, netdev_get_default_mib_dir());
    when_failed_trace(retval, exit, ERROR, "Could not scan default mib directory [amxo_parser_scan_mib_dir() failed]");

    // Apply NetDev.RTNLtrace
    netdev_root = amxd_dm_findf(netdev_get_dm(), "NetDev");
    if(netdev_root == NULL) {
        SAH_TRACEZ_ERROR(ME, "NetDev root object not found");
        goto exit;
    }
    trace = amxd_object_get_value(bool, netdev_root, "RTNLTrace", NULL);
    SAH_TRACEZ_INFO(ME, "RTNLTrace = %s", trace ? "true" : "false");
    rtnl_trace(trace);

    util_init();
    when_false(dm_link_init(), exit);
    when_false(addr_init(), exit);
    when_false(route_init(), exit);
    when_false(neigh_init(), exit);
    netdev_bridgetable_init();

    when_false_trace(dm_link_start(), exit, ERROR, "Error starting link");
    when_false_trace(addr_start(), exit, ERROR, "Error starting addr");
    when_false_trace(route_start(), exit, ERROR, "Error starting route");
    when_false_trace(neigh_start(), exit, ERROR, "Error starting neigh");
    when_false_trace(netdev_bridgetable_start(), exit, ERROR, "Error starting bridgetable");

    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int netdev_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    int retval = 0;

    amxo_connection_remove(netdev.parser, netdev.nlfd);
    rtnl_close(netdev.nlfd);
    // Unregisters all handlers
    rtnl_unregister_handlers();

    neigh_cleanup();
    dm_link_cleanup();
    route_cleanup();
    addr_cleanup();
    bridgetable_cleanup();

    // Handle all pending events before finishing cleanup
    while(amxp_signal_read() == 0) {
    }

    netdev.dm = NULL;
    netdev.parser = NULL;

    SAH_TRACEZ_OUT(ME);
    return retval;
}

void _syslog_event(const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Signal received - %s", sig_name);
    SAH_TRACEZ_INFO(ME, "Signal data = ");
    if(!amxc_var_is_null(data)) {
        amxc_var_log(data);
    }
}

void _on_rtnltrace_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    rtnl_trace(GETP_BOOL(data, "parameters.RTNLTrace.to"));
}

amxo_parser_t* netdev_get_parser(void) {
    return netdev.parser;
}

amxc_var_t* netdev_get_config(void) {
    return &(netdev.parser->config);
}

amxd_dm_t* netdev_get_dm(void) {
    return netdev.dm;
}

int netdev_get_nlfd(void) {
    return netdev.nlfd;
}

const char* netdev_get_default_mib_dir(void) {
    amxc_var_t* setting = amxo_parser_get_config(netdev_get_parser(), "default_mib_dir");
    return amxc_var_constcast(cstring_t, setting);
}

int _netdev_main(int reason,
                 amxd_dm_t* dm,
                 amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);

    int retval = 0;
    switch(reason) {
    case 0:     // AMXO_START
        retval = netdev_init(dm, parser);
        break;

    case 1:     // AMXO_STOP
        retval = netdev_cleanup();
        break;

    default:
        // not handled reason - should not fail
        break;
    }

    SAH_TRACEZ_OUT(ME);
    return retval;
}
