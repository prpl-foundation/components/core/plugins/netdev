/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include "netdev_netlink.h"
#include "netdev_util.h"

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link_neigh.h"

#define ME "netdev"

static amxc_llist_t neighs = {NULL, NULL};

static converter_t* family_converter = NULL;
static converter_t* flags_converter = NULL;
static converter_t* state_converter = NULL;

static bool should_emit_signal_from_neigh(const neigh_t* neigh);
static void emit_signal_from_neigh(neigh_t* neigh);
static void printNeighMsg(FILE* f, rtnlmsg_t* msg);

static rtnlhandler_t newNeighHandler = { RTM_NEWNEIGH, sizeof(struct ndmsg), handleNewNeigh, printNeighMsg };
static rtnlhandler_t delNeighHandler = { RTM_DELNEIGH, sizeof(struct ndmsg), handleDelNeigh, printNeighMsg };
static rtnlhandler_t getNeighHandler = { RTM_GETNEIGH, sizeof(struct ndmsg), NULL, printNeighMsg };

static void neigh_deferred_destroy(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    neigh_t* neigh = (neigh_t*) priv;

    when_null(neigh, exit);

    if(GETP_BOOL(data, "deleteObject")) {
        amxd_trans_t transaction;
        amxd_trans_init(&transaction);
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

        //Ensure that state is set to failed before deleting object.
        amxd_trans_select_pathf(&transaction, "NetDev.Link.%d.Neigh.dyn%u.", neigh->link, neigh->dyn_index);
        amxd_trans_set_value(cstring_t, &transaction, "State", "failed");
        amxd_trans_select_pathf(&transaction, "NetDev.Link.%d.Neigh.", neigh->link);
        amxd_trans_del_inst(&transaction, neigh->index, NULL);
        if(amxd_status_ok != amxd_trans_apply(&transaction, netdev_get_dm())) {
            SAH_TRACEZ_WARNING(ME, "Transaction failed");
        }
        neigh->state = NUD_FAILED;
        if(should_emit_signal_from_neigh(neigh)) {
            emit_signal_from_neigh(neigh);
        }
        amxd_trans_clean(&transaction);
    }
    amxc_llist_it_take(&neigh->it);
    free(neigh);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void neigh_destroy(neigh_t* neigh, bool deleteObject) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(neigh, exit, ERROR, "No neigh provided, can not remove neigh");
    when_false_trace(neigh->active, exit, INFO, "Neighbour already set to be destroyed");

    amxc_var_add_key(bool, &var_deferred_data, "deleteObject", deleteObject);
    // Set to inactive to prevent use between this point and deferred_destroy call
    neigh->active = false;
    if(neigh->object != NULL) {
        neigh->object->priv = NULL;
        neigh->object = NULL;
    }
    amxp_sigmngr_deferred_call(NULL, neigh_deferred_destroy, &var_deferred_data, neigh);

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return;
}

neigh_t* neigh_create(void) {
    neigh_t* neigh = (neigh_t*) calloc(1, sizeof(neigh_t));
    amxc_llist_append(&neighs, &neigh->it);
    neigh->active = true;
    neigh->reported_state = NUD_NONE;
    return neigh;
}

int neigh_copy(neigh_t* dst, neigh_t* src) {
    int rv = -1;

    when_null_trace(dst, exit, ERROR, "No destination neighbour struct provided");
    when_null_trace(src, exit, ERROR, "No source neighbour struct provided");

    dst->family = src->family;
    dst->link = src->link;
    dst->state = src->state;
    dst->flags = src->flags;
    memcpy(dst->dst, src->dst, addrlen(src->family));
    lladdr_copy(&dst->lladdr, &src->lladdr);

    rv = 0;
exit:
    return rv;
}

static bool neigh_match(neigh_t* n1, neigh_t* n2) {
    bool rv = false;

    when_null_trace(n1, exit, ERROR, "First neighbour is invalid, can not try to match");
    when_null_trace(n2, exit, ERROR, "Second neighbour is invalid, can not try to match");

    rv = (n1->family == n2->family) &&
        (n1->link == n2->link) &&
        (memcmp(n1->dst, n2->dst, addrlen(n1->family)) == 0);

exit:
    return rv;
}

neigh_t* neigh_find(neigh_t* ref) {
    neigh_t* neigh = NULL;
    when_null_trace(ref, exit, ERROR, "No reference provided to find neighbour");
    amxc_llist_iterate(it, &neighs) {
        neigh = amxc_container_of(it, neigh_t, it);
        if(neigh_match(neigh, ref) && (neigh->active == true)) {
            break;
        }
        neigh = NULL;
    }
exit:
    return neigh;
}

neigh_t* neigh_find_w_defroute(route_t* defroute, bool ipv4) {
    SAH_TRACEZ_IN(ME);
    neigh_t* neigh = NULL;
    when_null_trace(defroute, exit, ERROR, "No defroute provided to find neighbour");
    when_false_trace(defroute->default_route, exit, ERROR, "Not a default route");

    amxc_llist_iterate(it, &neighs) {
        neigh = amxc_container_of(it, neigh_t, it);
        if(neigh_defroute_match(neigh, defroute, ipv4)) {
            break;
        }
        neigh = NULL;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return neigh;
}

/**
 * Used to convert a rtnetlink message to a neigh struct
 * @param neigh empty neighbour struct to be filled with the data in the message.
 *  Do NOT provide a neigh that contains any data or that is put in the neighs list
 * (so no neighbour that was created with the neigh_create function)
 * @param msg rtnl message
 * @return 0 if ok, -1 in case of error
 */
int msg2neigh(neigh_t* neigh, rtnlmsg_t* msg) {
    int rv = -1;
    struct ndmsg* nd = (struct ndmsg*) rtnlmsg_hdr(msg);
    struct rtattr* rta = NULL;

    when_null_trace(neigh, exit, ERROR, "No neighbour structure provided");
    memset(neigh, 0, sizeof(neigh_t));
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    neigh->family = nd->ndm_family;
    neigh->link = nd->ndm_ifindex;
    neigh->state = nd->ndm_state;
    neigh->flags = nd->ndm_flags;

    rta = rtnlmsg_attr(msg, NDA_DST);
    if(rta != NULL) {
        memcpy(neigh->dst, RTA_DATA(rta), addrlen(neigh->family));
    }

    rta = rtnlmsg_attr(msg, NDA_LLADDR);
    if(rta != NULL) {
        lladdr_assign(&neigh->lladdr, RTA_PAYLOAD(rta), (unsigned char*) RTA_DATA(rta));
    }

    rv = 0;
exit:
    return rv;
}

static void new_neigh_deferred_transaction(const amxc_var_t* data, void* priv) {
    amxd_status_t ret = amxd_status_invalid_value;
    deferred_trans_t* deferred_trans = (deferred_trans_t*) priv;
    amxd_trans_t* transaction = NULL;
    neigh_t* neigh = NULL;

    when_null_trace(deferred_trans, exit, ERROR, "No deferred transaction provided");
    transaction = deferred_trans->transaction;
    neigh = (neigh_t*) deferred_trans->priv;
    when_null_trace(transaction, exit, ERROR, "No transaction provided");
    when_null_trace(neigh, exit, ERROR, "No addr structure provided");
    when_false_trace(neigh->active, exit, INFO, "Neighbour set to be destroyed, not updating it anymore");

    ret = amxd_trans_apply(transaction, netdev_get_dm());
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", ret);
    }

    if(GETP_BOOL(data, "new_instance")) {
        neigh->object = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.Neigh.%s", neigh->link,
                                      GETP_CHAR(data, "pathbuf"));
        if(neigh->object == NULL) {
            neigh_destroy(neigh, false);
            goto exit;
        }
        neigh->object->priv = neigh;
        neigh->index = neigh->object->index;
    }
    SAH_TRACEZ_INFO(ME, "Deferred neighbour transaction successful");

exit:
    deferred_trans_clean(&deferred_trans);
}

amxd_trans_t* create_transaction_from_neigh(neigh_t* neigh, bool new_inst) {
    SAH_TRACEZ_IN(ME);
    char pathbuf[40];
    amxd_trans_t* trans = NULL;

    when_null_trace(neigh, exit, ERROR, "No neighbour provide to create transaction");

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(trans, amxd_tattr_change_priv, true);

    if(new_inst) {
        sprintf(pathbuf, "dyn%u", neigh->dyn_index);
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.Neigh", neigh->link);
        amxd_trans_add_inst(trans, 0, pathbuf);
    } else {
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.Neigh.dyn%u.", neigh->link, neigh->dyn_index);
    }

    amxd_trans_set_value(cstring_t, trans, "Family", convert_bin2str(family_converter, neigh->family));
    amxd_trans_set_value(cstring_t, trans, "Flags", convert_bin2str(flags_converter, neigh->flags));
    amxd_trans_set_value(cstring_t, trans, "Dst", addr2str(neigh->family, neigh->dst, false));
    amxd_trans_set_value(cstring_t, trans, "LLAddress", lladdr2str(&neigh->lladdr));
    amxd_trans_set_value(cstring_t, trans, "State", convert_bin2str(state_converter, neigh->state));

exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

static bool should_emit_signal_from_neigh(const neigh_t* neigh) {
    // These notifications cause a lot of CPU usage on components that have subscriptions to NetDev,
    // even if these subscriptions are not about these notifications.
    // So we limit sending these notifications strictly to only the cases that they are used.
    //
    // Be very careful when extending the cases in which the notification is sent,
    // and if you do it: benchmark with a network with 100 devices if some components that depend
    // on NetDev start to have CPU load.
    return (neigh != NULL) && (neigh->reported_state != neigh->state) && (
        ((neigh->family == AF_INET) && (neigh->state == NUD_REACHABLE) && (neigh->reported_state != NUD_STALE)) ||
        ((neigh->family == AF_INET6) && (neigh->state == NUD_REACHABLE)) ||
        ((neigh->family == AF_INET6) && (neigh->reported_state == NUD_REACHABLE) && (neigh->state != NUD_REACHABLE))
        );
}

/**
 * Sends a signal (from the event loop, so not immediately) describing the neigh message.
 */
static void emit_signal_from_neigh(neigh_t* neigh) {
    amxc_var_t notification_data;
    amxc_var_t* neighbor_data = NULL;
    amxd_object_t* object = NULL;
    amxc_var_init(&notification_data);

    when_null_trace(neigh, exit, ERROR, "NULL argument 'neigh'");

    object = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.Neigh", neigh->link);
    when_null_trace(object, exit, ERROR, "Object for sending notification not found");

    amxc_var_set_type(&notification_data, AMXC_VAR_ID_HTABLE);
    neighbor_data = amxc_var_add_new_key(&notification_data, "NeighborData");
    amxc_var_set_type(neighbor_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, neighbor_data, "Family", convert_bin2str(family_converter, neigh->family));
    amxc_var_add_key(cstring_t, neighbor_data, "Flags", convert_bin2str(flags_converter, neigh->flags));
    amxc_var_add_key(cstring_t, neighbor_data, "Dst", addr2str(neigh->family, neigh->dst, false));
    amxc_var_add_key(cstring_t, neighbor_data, "LLAddress", lladdr2str(&neigh->lladdr));
    amxc_var_add_key(cstring_t, neighbor_data, "State", convert_bin2str(state_converter, neigh->state));
    amxc_var_add_key(cstring_t, neighbor_data, "OldState", convert_bin2str(state_converter, neigh->reported_state));

    amxd_object_emit_signal(object, "Neighbor!", &notification_data);
    neigh->reported_state = neigh->state;

exit:
    amxc_var_clean(&notification_data);
}

int handleNewNeigh(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    neigh_t ws;
    neigh_t* neigh = NULL;
    route_t* route = NULL;
    struct ndmsg* nd = (struct ndmsg*) rtnlmsg_hdr(msg);
    char pathbuf[40];
    deferred_trans_t* deferred_trans = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    // Don't show multicast/broadcast/... entries
    when_true_status(nd->ndm_type != RTN_UNICAST, exit, rv = 0);
    when_failed_trace(msg2neigh(&ws, msg), exit, ERROR, "Failed to convert msg to neighbour");
    neigh = neigh_find(&ws);
    if(neigh) {
        SAH_TRACEZ_INFO(ME, "Existing 'neigh' found");
    } else {
        SAH_TRACEZ_INFO(ME, "Creating new 'neigh'; NetDev.Link.%d.Neigh", ws.link);
        neigh = neigh_create();
        neigh->dyn_index = ({static unsigned counter = 0; counter++;});
        sprintf(pathbuf, "dyn%u", neigh->dyn_index);

        amxc_var_add_key(bool, &var_deferred_data, "new_instance", true);
        amxc_var_add_key(cstring_t, &var_deferred_data, "pathbuf", pathbuf);
        SAH_TRACEZ_INFO(ME, "pathbuf = %s", pathbuf);
    }
    neigh_copy(neigh, &ws);

    transaction = create_transaction_from_neigh(neigh, GET_BOOL(&var_deferred_data, "new_instance"));

    deferred_trans_create(&deferred_trans, transaction, neigh);
    rv = amxp_sigmngr_deferred_call(NULL, new_neigh_deferred_transaction, &var_deferred_data, deferred_trans);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to defer the new neighbour transaction");
        deferred_trans_clean(&deferred_trans);
    }

    if(should_emit_signal_from_neigh(neigh)) {
        emit_signal_from_neigh(neigh);
    }

    // Throw the event in case a neighbour makes a change related to the default route
    route = neigh_bind_to_defroute(neigh, true);
    if(route != NULL) {
        when_failed_trace(default_route_event(route, true), exit, INFO, "No default route event to send for IPv4");
    }
    route = neigh_bind_to_defroute(neigh, false);
    if(route != NULL) {
        when_failed_trace(default_route_event(route, true), exit, INFO, "No default route event to send for IPv6");
    }

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int handleDelNeigh(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    neigh_t ws;
    neigh_t* neigh = NULL;
    route_t* route = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    when_failed_trace(msg2neigh(&ws, msg), exit, ERROR, "Failed to convert msg to neighbour");
    neigh = neigh_find(&ws);

    // find default route to associate the neighbour with
    route = neigh_bind_to_defroute(neigh, true);
    // throw the event in case a new neighbour is linked
    if(route != NULL) {
        //Test if the route is default or not. If yes, send event
        if(default_route_event(route, false) != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not send default route event");
        }
        route->def_neigh = NULL;
    }

    // find default route to associate the neighbour with
    route = neigh_bind_to_defroute(neigh, false);
    // throw the event in case a new neighbour is linked
    if(route != NULL) {
        //Test if the route is default or not. If yes, send event
        if(default_route_event(route, false) != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not send default route event");
        }
        route->def_neigh = NULL;
    }

    when_null_trace(neigh, exit, ERROR, "No matching neighbour found to delete");
    neigh_destroy(neigh, true);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void printNeighMsg(FILE* f, rtnlmsg_t* msg) {
    struct ndmsg* nd = NULL;
    struct rtattr* rta = NULL;
    unsigned int rtalen = 0;
    fprintf(f, "%s{",
            rtnlmsg_type(msg) == RTM_NEWNEIGH ? "RTM_NEWNEIGH" :
            rtnlmsg_type(msg) == RTM_DELNEIGH ? "RTM_DELNEIGH" :
            rtnlmsg_type(msg) == RTM_GETNEIGH ? "RTM_GETNEIGH" : "<UNKNOWN>");
    nd = (struct ndmsg*) rtnlmsg_hdr(msg);
    fprintf(f, " family=%u(%s)", nd->ndm_family, nd->ndm_family ? convert_bin2str(family_converter, nd->ndm_family) : "unspec");
    fprintf(f, " ifindex=%d", nd->ndm_ifindex);
    fprintf(f, " state=%u(%s)", nd->ndm_state, convert_bin2str(state_converter, nd->ndm_state));
    fprintf(f, " flags=%u(%s)", nd->ndm_flags, convert_bin2str(flags_converter, nd->ndm_flags));
    fprintf(f, " type=%u", nd->ndm_type);
    rtalen = rtnlmsg_attrLen(msg);
    for(rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        switch(rta->rta_type) {
        case NDA_DST:    fprintf(f, " dst=%s", addr2str(nd->ndm_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case NDA_LLADDR: fprintf(f, " lladdr=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char*) RTA_DATA(rta)))); break;
        case NDA_CACHEINFO: {
            struct nda_cacheinfo* ci = (struct nda_cacheinfo*) RTA_DATA(rta);
            fprintf(f, " cacheinfo{confirmed=%lu used=%lu updated=%lu refcnt=%lu}",
                    (unsigned long) ci->ndm_confirmed, (unsigned long) ci->ndm_used,
                    (unsigned long) ci->ndm_updated, (unsigned long) ci->ndm_refcnt);
            break;
        }
        default:
            fprintf(f, " attr[%u](%" PRIu32 "byte%s)", rta->rta_type, (uint32_t) RTA_PAYLOAD(rta), RTA_PAYLOAD(rta) == 1 ? "" : "s");
            break;
        }
    }
    fprintf(f, " }");
}

converter_t* init_neigh_family_converter(void) {
    converter_t* converter = converter_create("family", converter_attribute_none);
    converter_loadEntry(converter, AF_INET, "ipv4");
    converter_loadEntry(converter, AF_INET6, "ipv6");
    converter_loadEntry(converter, AF_BRIDGE, "bridge");
    return converter;
}

converter_t* init_neigh_flags_converter(void) {
    converter_t* converter = converter_create("neigh_flags", converter_attribute_flags);
    converter_loadEntry(converter, NTF_PROXY, "proxy");
    converter_loadEntry(converter, NTF_ROUTER, "router");
    return converter;
}

converter_t* init_neigh_state_converter(void) {
    converter_t* converter = converter_create("neigh_state", converter_attribute_flags);
    converter_loadEntry(converter, NUD_INCOMPLETE, "incomplete");
    converter_loadEntry(converter, NUD_REACHABLE, "reachable");
    converter_loadEntry(converter, NUD_STALE, "stale");
    converter_loadEntry(converter, NUD_DELAY, "delay");
    converter_loadEntry(converter, NUD_PROBE, "probe");
    converter_loadEntry(converter, NUD_FAILED, "failed");
    converter_loadEntry(converter, NUD_NOARP, "noarp");
    converter_loadEntry(converter, NUD_PERMANENT, "permanent");
    return converter;
}

void neigh_init_converters(void) {
    family_converter = init_neigh_family_converter();
    flags_converter = init_neigh_flags_converter();
    state_converter = init_neigh_state_converter();
}

bool neigh_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxd_param_t* parameter = NULL;
    amxd_object_t* netdev_neighs = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.Neigh");

    neigh_init_converters();

    parameter = amxd_object_get_param_def(netdev_neighs, "Family");
    rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, family_converter);
    parameter = amxd_object_get_param_def(netdev_neighs, "Flags");
    rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, flags_converter);
    parameter = amxd_object_get_param_def(netdev_neighs, "State");
    rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, state_converter);

    rtnl_register_handler(&newNeighHandler);
    rtnl_register_handler(&delNeighHandler);
    rtnl_register_handler(&getNeighHandler);

    SAH_TRACEZ_OUT(ME);
    return (rv == amxd_status_ok);
}

bool neigh_start(void) {
    SAH_TRACEZ_IN(ME);
    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_GETNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST | NLM_F_DUMP);

    if(!rtnl_send(netdev_get_nlfd(), msg)) {
        return false;
    }

    if(!rtnl_read(netdev_get_nlfd(), msg, true)) {
        return false;
    }
    SAH_TRACEZ_OUT(ME);
    return true;
}

void neigh_cleanup(void) {
    while(!amxc_llist_is_empty(&neighs)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&neighs);
        neigh_t* neigh = amxc_container_of(it, neigh_t, it);
        neigh_destroy(neigh, false);
        amxc_llist_it_take(it);
    }

    converter_destroy(&family_converter);
    converter_destroy(&flags_converter);
    converter_destroy(&state_converter);
}

amxd_status_t _neigh_destroy(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    neigh_t* neigh = NULL;
    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if((object == NULL) || (amxd_object_get_type(object) == amxd_object_template)) {
        goto exit;
    }

    neigh = (neigh_t*) object->priv;
    when_null(neigh, exit);
    neigh_destroy(neigh, false);

exit:
    return status;
}
