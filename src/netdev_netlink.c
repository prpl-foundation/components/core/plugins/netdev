/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_action.h>

#include "netdev.h"
#include "netdev_netlink.h"

#define ME "netlink"

typedef struct {
    amxc_llist_it_t it;
    rtnlhandler_t handler;
} handler_t;

static amxc_llist_t handlers = {NULL, NULL};
static bool tracing_enabled = true;        // Can be set via NetDev.RTNLTrace = 0|1
static bool tracing_raw_enabled = false;   // for debugging only, can only be set at compile time

static uint32_t msgs_read = 0;
static uint32_t msgs_multi_part_read = 0;
static uint32_t msgs_error = 0;
static uint32_t msgs_handled = 0;
static uint32_t msgs_noop = 0;
static uint32_t msgs_done = 0;
static uint32_t msgs_resend = 0;
static uint32_t msgs_lost = 0;

#define historylen 16
#define historybufsize 1024

// history: used to store the last 'historylen' rtnl messages. Only used in diagnostic functions

static union {
    char raw[historybufsize];
    rtnlmsg_t msg;
} history[historylen] = {
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[0].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[1].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[2].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[3].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[4].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[5].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[6].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[7].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[8].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[9].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[10].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[11].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[12].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[13].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[14].msg.buf}},
    {.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) history[15].msg.buf}},
};
static int historycur = 0;
static int historymax = 0;

static void handler_destroy(amxc_llist_it_t* it) {
    handler_t* h = amxc_container_of(it, handler_t, it);
    free(h);
}

static const char* rtnlmsg_print(rtnlmsg_t* msg) {
#define MSGBUF_LEN 4096
    //static char msgbuf[1024];
    static char msgbuf[MSGBUF_LEN];
    FILE* f = NULL;
    handler_t* h = NULL;

    if(!msg) {
        return "<< message lost >>";
    }

    if(tracing_raw_enabled) {
        printf("\n\n");
        int i, j;
        unsigned char* raw = (unsigned char*) msg->nlmsg;
        int len = msg->nlmsg->nlmsg_len;
        for(i = 0; i < len; i += 16) {
            printf("%04x: ", i);
            for(j = i; j < i + 16 && j < len; j++) {
                printf("%02x ", raw[j]);
            }
            for(; j < i + 16; j++) {
                printf("   ");
            }
            printf("| ");
            for(j = i; j < i + 16 && j < len; j++) {
                printf("%c", raw[j] < 32 ? '.' : raw[j]);
            }
            printf("\n");
        }
    }

    memset(msgbuf, 0, sizeof(msgbuf));
    f = fmemopen(msgbuf, MSGBUF_LEN, "w");
    amxc_llist_iterate(it, &handlers) {
        h = amxc_container_of(it, handler_t, it);
        if((h->handler.type == msg->nlmsg->nlmsg_type) && h->handler.print) {
            msg->hdrlen = h->handler.hdrlen;
            h->handler.print(f, msg);
            msgbuf[MSGBUF_LEN - 1] = '\0';
            break;
        }
    }
    fclose(f);

    if(!h) {
        sprintf(msgbuf, "{ msgtype=%d }", msg->nlmsg->nlmsg_type);
    }
    return msgbuf;
}

bool rtnlmsg_store(rtnlmsg_t* msg) {
    bool rv = false;

    when_null_trace(msg, exit, ERROR, "Failed to store message, no message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Failed to store message, message contains no netlink message");

    history[historycur].msg.hdrlen = msg->hdrlen;
    memcpy(history[historycur].msg.buf, msg->nlmsg, msg->nlmsg->nlmsg_len);
    historycur++;
    if(historymax < historylen) {
        historymax++;
    }
    historycur %= historylen;

    rv = true;

exit:
    return rv;
}

rtnlmsg_t* rtnlmsg_load(struct nlmsghdr* nlmsg) {
    int i;

    when_null_trace(nlmsg, exit, ERROR, "Failed to load message, netlink message provided");

    for(i = 0; i < historylen && i < historymax; i++) {
        if(history[i].msg.nlmsg->nlmsg_seq == nlmsg->nlmsg_seq) {
            return &history[i].msg;
        }
    }

exit:
    return NULL;
}

int rtnl_open(void) {
    SAH_TRACEZ_IN(ME);
    int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    struct sockaddr_nl bindaddr;

    when_false_trace(fd > 0, exit, ERROR, "socket creation failed - %s", strerror(errno));

    memset(&bindaddr, 0, sizeof(struct sockaddr_nl));
    bindaddr.nl_family = AF_NETLINK;
    bindaddr.nl_pid = getpid();
    bindaddr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR |
        RTMGRP_IPV4_ROUTE | RTMGRP_IPV6_ROUTE | RTMGRP_NEIGH;
    if(bind(fd, (struct sockaddr*) &bindaddr, sizeof(struct sockaddr_nl)) < 0) {
        SAH_TRACEZ_ERROR(ME, "bind failed - %s", strerror(errno));
        return -1;
    }

exit:
    SAH_TRACEZ_INFO(ME, "rtnl_open returns netlink socket fd = %d", fd);
    SAH_TRACEZ_OUT(ME);
    return fd;
}

/* rtnl_cleanup */
void rtnl_close(int fd) {
    close(fd);
}

/* rtnl_registerHandler */
int rtnl_register_handler(rtnlhandler_t* handler) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    handler_t* h = NULL;

    when_null_trace(handler, exit, ERROR, "No handler provided to register");
    h = (handler_t*) calloc(1, sizeof(handler_t));
    when_null_trace(h, exit, ERROR, "Failed to allocate memory for new handler");

    h->handler.type = handler->type;
    h->handler.hdrlen = handler->hdrlen;
    h->handler.handle = handler->handle;
    h->handler.print = handler->print;
    rv = amxc_llist_append(&handlers, &h->it);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/* rtnl_unregisterHandler */
void rtnl_unregister_handlers(void) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_clean(&handlers, handler_destroy);
    SAH_TRACEZ_OUT(ME);
}

bool rtnl_send(int fd, rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    struct sockaddr_nl addr;
    bool retval = false;

    when_false_trace(fd > 0, exit, ERROR, "Invalid file descriptor");
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    msg->nlmsg->nlmsg_seq = ({
        static int nlmsg_seq = 0;
        ++nlmsg_seq;
    });
    msg->nlmsg->nlmsg_pid = 0;
    memset(&addr, 0, sizeof(struct sockaddr_nl));
    addr.nl_family = AF_NETLINK;

    struct iovec iov = {msg->nlmsg, msg->nlmsg->nlmsg_len};
    struct msghdr msghdr = {
        .msg_name = &addr,
        .msg_namelen = sizeof(struct sockaddr_nl),
        .msg_iov = &iov,
        .msg_iovlen = 1,
        .msg_control = NULL,
        .msg_controllen = 0,
        .msg_flags = 0
    };

    if(tracing_enabled) {
        SAH_TRACEZ_NOTICE(ME, "Send message #%lu: %s", (unsigned long) msg->nlmsg->nlmsg_seq, rtnlmsg_print(msg));
    }

    rtnlmsg_store(msg);
    when_false_trace(sendmsg(fd, &msghdr, 0) > 0, exit, ERROR, "sendmsg failed - %s", strerror(errno));
    retval = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

bool rtnl_read(int fd, rtnlmsg_t* msg, bool blocking) {
    SAH_TRACEZ_IN(ME);

    ssize_t buflen;
    unsigned int ubuflen;
    bool done = false;
    bool multi = false;
    bool retval = false;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    when_false_trace(fd > 0, exit, ERROR, "No valid file descriptor provided");

    do {
        buflen = recv(fd, msg->buf, msg->buflen - offsetof(rtnlmsg_t, buf), 0);
        if(buflen < 0) {
            if((errno == EINTR) || (errno == EAGAIN)) {
                SAH_TRACEZ_NOTICE(ME, "recv() ignore netlink message %s (%d)", strerror(errno), errno);
                continue;
            }
            SAH_TRACEZ_ERROR(ME, "recv() netlink receive error %s (%d)", strerror(errno), errno);
            if(errno == ENOBUFS) {
                SAH_TRACEZ_NOTICE(ME, "recv() warning %s (%d)", strerror(errno), errno);
                continue;
            }
            goto exit;
        }

        if(buflen == 0) {
            SAH_TRACEZ_ERROR(ME, "EOF on netlink");
            goto exit;
        }

        ubuflen = (unsigned) buflen;
        for(msg->nlmsg = (struct nlmsghdr*) msg->buf;
            NLMSG_OK(msg->nlmsg, ubuflen);
            msg->nlmsg = NLMSG_NEXT(msg->nlmsg, ubuflen)) {     // process all messages

            msgs_read++;
            if(msg->nlmsg->nlmsg_flags & NLM_F_MULTI) {
                msgs_multi_part_read++;
                multi = true; // mark as multipart message
            }

            switch(msg->nlmsg->nlmsg_type) {
            case NLMSG_ERROR: {
                // find the original request that matches this errored reply in the history.
                struct nlmsgerr* nlerr = (struct nlmsgerr*) NLMSG_DATA(msg->nlmsg);
                rtnlmsg_t* origmsg = rtnlmsg_load(&nlerr->msg);
                msgs_error++;
                if(origmsg && ( nlerr->error == -EBUSY)) {
                    // resend original rtnl msg
                    msgs_resend++;
                    rtnl_send(fd, origmsg);
                } else {
                    msgs_lost++;
                    SAH_TRACEZ_ERROR(ME, "Error %d (%s) received for request #%lu : %s",
                                     -nlerr->error, strerror(-nlerr->error),
                                     (unsigned long) nlerr->msg.nlmsg_seq,
                                     rtnlmsg_print(origmsg));
                }
                break;
            }
            case NLMSG_NOOP:
                msgs_noop++;
                break;
            case NLMSG_DONE: // end of multipart message
                msgs_done++;
                done = true;
                break;
            default:
                if(tracing_enabled) {
                    SAH_TRACEZ_NOTICE(ME, "Received message #%lu : %s", (unsigned long) msg->nlmsg->nlmsg_seq, rtnlmsg_print(msg));
                }
                bool handler_called = false;
                amxc_llist_iterate(it, &handlers) {
                    handler_t* h = amxc_container_of(it, handler_t, it);
                    if(h->handler.type == msg->nlmsg->nlmsg_type) {
                        msg->hdrlen = h->handler.hdrlen;
                        if(h->handler.handle) {
                            h->handler.handle(msg);
                            handler_called = true;
                            msgs_handled++;
                        }
                    }
                }
                SAH_TRACEZ_INFO(ME, "handler was %s", handler_called ? "called" : "NOT called");
            }
        }
    } while(blocking && !done && multi);

    retval = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}


void rtnl_trace(bool enable) {
    SAH_TRACEZ_INFO(ME, "rtnl_trace: enabled = %s", enable ? "true" : "false");
    tracing_enabled = enable;
}

void get_rtnl_read_stats(amxc_var_t* const retval) {
    amxc_var_t* val = NULL;

    val = amxc_var_get_key(retval, "NetLinkMsgsTotal", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_read);
    val = amxc_var_get_key(retval, "NetLinkMsgsPartial", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_multi_part_read);
    val = amxc_var_get_key(retval, "NetLinkMsgsError", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_error);
    val = amxc_var_get_key(retval, "NetLinkMsgsHandled", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_handled);
    val = amxc_var_get_key(retval, "NetLinkMsgsNoOp", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_noop);
    val = amxc_var_get_key(retval, "NetLinkMsgsMultiPartDone", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_done);
    val = amxc_var_get_key(retval, "NetLinkMsgsErrorLost", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_lost);
    val = amxc_var_get_key(retval, "NetLinkMsgsErrorResend", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_uint32_t(val, msgs_resend);
}

amxd_status_t _netlink_msgs_read(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_ok;


    status = amxd_action_object_read(object, param, reason, args, retval, priv);

    when_true(status != amxd_status_ok &&
              status != amxd_status_parameter_not_found,
              exit);

    get_rtnl_read_stats(retval);

exit:
    return status;
}
