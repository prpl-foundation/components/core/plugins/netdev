/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stddef.h>
#include <string.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include "netdev.h"
#include "netdev_netlink.h"

#define ME "netlink"

static union {
    char raw[8192];
    rtnlmsg_t msg;
} static_rtnlmsg_buffer = {.msg = {.buflen = 8192, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) static_rtnlmsg_buffer.msg.buf}};


rtnlmsg_t* rtnlmsg_instance(void) {
    return &static_rtnlmsg_buffer.msg;
}

void rtnlmsg_initialize(rtnlmsg_t* msg, unsigned short type, unsigned short hdrlen, unsigned short flags) {
    size_t buflen = sizeof(static_rtnlmsg_buffer);
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");

    memset(msg, 0, buflen);
    msg->buflen = buflen;
    msg->hdrlen = hdrlen;
    msg->nlmsg = (struct nlmsghdr*) msg->buf;
    msg->nlmsg->nlmsg_len = NLMSG_LENGTH(hdrlen);
    msg->nlmsg->nlmsg_type = type;
    msg->nlmsg->nlmsg_flags = flags;
exit:
    return;
}

unsigned short rtnlmsg_type(rtnlmsg_t* msg) {
    unsigned short rv = 0;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    rv = msg->nlmsg->nlmsg_type;

exit:
    return rv;
}

int rtnlmsg_buflen(rtnlmsg_t* msg) {
    unsigned short rv = 0;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    rv = msg->buflen;

exit:
    return rv;
}

struct nlmsghdr* rtnlmsg_nlhdr(rtnlmsg_t* msg) {
    struct nlmsghdr* rv = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    rv = msg->nlmsg;

exit:
    return rv;
}

void* rtnlmsg_hdr(rtnlmsg_t* msg) {
    void* rv = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    rv = NLMSG_DATA(msg->nlmsg);

exit:
    return rv;
}

struct rtattr* rtnlmsg_attr(rtnlmsg_t* msg, unsigned short type) {
    struct rtattr* rta = NULL;
    unsigned int rtalen = 0;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    rtalen = NLMSG_PAYLOAD(msg->nlmsg, msg->hdrlen);
    for(rta = (struct rtattr*) (NLMSG_DATA(msg->nlmsg) + NLMSG_ALIGN(msg->hdrlen)); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        if(rta->rta_type == type) {
            goto exit;
        }
    }
    rta = NULL;

exit:
    return rta;
}

struct rtattr* rtnlmsg_attrFirst(rtnlmsg_t* msg) {
    struct rtattr* rv = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    rv = (NLMSG_DATA(msg->nlmsg) + NLMSG_ALIGN(msg->hdrlen));

exit:
    return rv;
}

unsigned int rtnlmsg_attrLen(rtnlmsg_t* msg) {
    unsigned int rv = 0;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    rv = NLMSG_PAYLOAD(msg->nlmsg, msg->hdrlen);

exit:
    return rv;
}

void rtnlmsg_addAttr(rtnlmsg_t* msg, unsigned short type, unsigned short len, const void* data) {
    struct rtattr* rta = NULL;
    unsigned short rtalen = RTA_LENGTH(len);

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    when_true_trace(NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen + ((char*) msg->nlmsg - (char*) msg) > msg->buflen,
                    exit, ERROR, "Not enough room to add attribute %d to netlink message", type);

    rta = (struct rtattr*) (((char*) msg->nlmsg) + NLMSG_ALIGN(msg->nlmsg->nlmsg_len));
    rta->rta_type = type;
    rta->rta_len = rtalen;
    if(len > 0) {
        memcpy(RTA_DATA(rta), data, len);
    }
    msg->nlmsg->nlmsg_len = NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen;

exit:
    return;
}