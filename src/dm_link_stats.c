/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>

#include "netdev.h"

#include "dm_netdev.h"
#include "dm_link.h"
#include "dm_link_stats.h"

#define ME "netdev"

#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
static stat_ref_t refs[] = {
    {"RxPackets", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_packets) },
    {"TxPackets", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_packets)},
    {"RxBytes", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_bytes)},
    {"TxBytes", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_bytes)},
    {"RxErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_errors)},
    {"TxErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_errors)},
    {"RxDropped", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_dropped)},
    {"TxDropped", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_dropped)},
    {"Multicast", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, multicast)},
    {"Collisions", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, collisions)},
    {"RxLengthErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_length_errors)},
    {"RxOverErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_over_errors)},
    {"RxCrcErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_crc_errors)},
    {"RxFrameErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_frame_errors)},
    {"RxFifoErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_fifo_errors)},
    {"RxMissedErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, rx_missed_errors)},
    {"TxAbortedErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_aborted_errors)},
    {"TxCarrierErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_carrier_errors)},
    {"TxFifoErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_fifo_errors)},
    {"TxHeartbeatErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_heartbeat_errors)},
    {"TxWindowErrors", AMXC_VAR_ID_UINT64, offsetof(struct rtnl_link_stats64, tx_window_errors)},
    { NULL, 0, 0 }
};
#else
static stat_ref_t refs[] = {
    {"RxPackets", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_packets) },
    {"TxPackets", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_packets)},
    {"RxBytes", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_bytes)},
    {"TxBytes", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_bytes)},
    {"RxErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_errors)},
    {"TxErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_errors)},
    {"RxDropped", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_dropped)},
    {"TxDropped", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_dropped)},
    {"Multicast", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, multicast)},
    {"Collisions", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, collisions)},
    {"RxLengthErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_length_errors)},
    {"RxOverErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_over_errors)},
    {"RxCrcErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_crc_errors)},
    {"RxFrameErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_frame_errors)},
    {"RxFifoErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_fifo_errors)},
    {"RxMissedErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, rx_missed_errors)},
    {"TxAbortedErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_aborted_errors)},
    {"TxCarrierErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_carrier_errors)},
    {"TxFifoErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_fifo_errors)},
    {"TxHeartbeatErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_heartbeat_errors)},
    {"TxWindowErrors", AMXC_VAR_ID_UINT32, offsetof(struct rtnl_link_stats, tx_window_errors)},
    { NULL, 0, 0 }
};
#endif

#define STATS_VAL(type, addr, offset) \
    ((addr == NULL) ? 0 :*((type*) (((char*) addr) + offset)))

static amxc_var_t* describe_statistic(amxc_var_t* ht_params,
                                      void* data,
                                      stat_ref_t* ref) {
    amxc_var_t* value = NULL;
    amxc_var_t* param = NULL;
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    struct rtnl_link_stats64* stats = (struct rtnl_link_stats64*) data;
#else
    struct rtnl_link_stats* stats = (struct rtnl_link_stats*) data;
#endif

    param = amxc_var_add_key(amxc_htable_t, ht_params, ref->name, NULL);

    amxd_param_build_description(param, ref->name, ref->type,
                                 SET_BIT(amxd_pattr_read_only) |
                                 SET_BIT(amxd_pattr_variable),
                                 NULL);

    value = GET_ARG(param, "value");
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    amxc_var_set(uint64_t, value, STATS_VAL(uint64_t, stats, ref->offset));
#else
    amxc_var_set(uint32_t, value, STATS_VAL(uint32_t, stats, ref->offset));
#endif

    return param;
}

static amxd_status_t get_value_statistics(amxc_var_t* const retval,
                                          const char* filter,
                                          void* data) {
    amxd_status_t status = amxd_status_file_not_found;
    amxp_expr_t expr;
    amxc_var_t description;
    amxp_expr_status_t expr_status = amxp_expr_status_ok;

    amxc_var_init(&description);
    amxc_var_set_type(&description, AMXC_VAR_ID_HTABLE);
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    struct rtnl_link_stats64* stats = (struct rtnl_link_stats64*) data;
#else
    struct rtnl_link_stats* stats = (struct rtnl_link_stats*) data;
#endif

    if(filter != NULL) {
        if(amxp_expr_init(&expr, filter) != 0) {
            goto exit;
        }
    }

    for(int i = 0; refs[i].name != NULL; i++) {
        if(filter != NULL) {
            amxc_var_t* param = describe_statistic(&description, stats, &refs[i]);
            if(!amxp_expr_eval_var(&expr, param, &expr_status)) {
                amxc_var_delete(&param);
                continue;
            }
            amxc_var_delete(&param);
        }
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
        amxc_var_add_key(uint64_t, retval, refs[i].name,
                         STATS_VAL(uint64_t, stats, refs[i].offset));
#else
        amxc_var_add_key(uint32_t, retval, refs[i].name,
                         STATS_VAL(uint32_t, stats, refs[i].offset));
#endif
        status = amxd_status_ok;
    }

    if(filter != NULL) {
        amxp_expr_clean(&expr);
    }

exit:
    amxc_var_clean(&description);
    return status;
}

amxd_status_t _stats_read(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_read(object, param, reason, args, retval, priv);
    amxc_string_t filter;
    amxc_string_init(&filter, 0);

    if(status != amxd_status_ok) {
        goto exit;
    }
    read_stats(object);     // Read the current statistics using the netlink-kernel interface
    status = amxd_action_object_read_filter(&filter, args);
    if(status == 0) {
        status = get_value_statistics(retval, amxc_string_get(&filter, 0), object->priv);
    }

exit:
    amxc_string_clean(&filter);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _stats_list(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_list(object, param, reason, args, retval, priv);
    amxc_var_t* params = NULL;
    if(status != amxd_status_ok) {
        goto exit;
    }
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; refs[i].name != NULL; i++) {
        amxc_var_add(cstring_t, params, refs[i].name);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _stats_describe(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_describe(object, param, reason, args, retval, priv);
    amxc_var_t* params = NULL;
    if(status != amxd_status_ok) {
        goto exit;
    }
    read_stats(object);
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; refs[i].name != NULL; i++) {
        describe_statistic(params, object->priv, &refs[i]);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _stats_destroy(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    struct rtnl_link_stats64* stats = NULL;
#else
    struct rtnl_link_stats* stats = NULL;
#endif

    when_null(object, exit);

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    stats = (struct rtnl_link_stats64*) object->priv;
#else
    stats = (struct rtnl_link_stats*) object->priv;
#endif

    free(stats);
    object->priv = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}
