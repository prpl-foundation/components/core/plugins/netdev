/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "netdev_parseresult.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <libmnl/libmnl.h>

#include "netdev_netlink.h"
#include "netdev_util.h"
#include <malloc.h>

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link_neigh.h"

#define ME "netdev"

/**
 *
 * Implements @see mnl_attr_cb_t
 */
static int s_parse_attr_callback(const struct nlattr* attr, void* data) {
    /* skip unsupported attribute in user-space */
    if(mnl_attr_type_valid(attr, NDA_MAX) < 0) {
        return MNL_CB_OK;
    }

    const struct nlattr** attrs = (const struct nlattr**) data;

    attrs[attr->nla_type] = attr;

    return MNL_CB_OK;
}

netdev_parseresult_bridgetable_t* netdev_parseresult_bridgetable_parse(const struct nlmsghdr* header) {
    netdev_parseresult_bridgetable_t* parseresult = NULL;
    struct nlattr* attrs[__NDA_MAX] = {0};
    const struct ndmsg* data = NULL;
    const unsigned char* mac_bin = NULL;
    uint16_t mac_bin_len = 0;
    const char* mac_str = NULL;
    bool is_local;

    // get netlink message parts and do checks on them
    when_null_trace(header, abort, ERROR, "NULL");
    when_false_trace(mnl_nlmsg_get_payload_len(header) >= sizeof(struct ndmsg), abort,
                     ERROR, "Received message too short");
    when_false_trace(header->nlmsg_type == RTM_NEWNEIGH || header->nlmsg_type == RTM_DELNEIGH, abort,
                     ERROR, "Unexpected message type");
    data = (const struct ndmsg*) mnl_nlmsg_get_payload(header);
    when_false(data->ndm_family == PF_BRIDGE, abort); // message not for us
    mnl_attr_parse(header, sizeof(struct ndmsg), s_parse_attr_callback, attrs);
    when_null_trace(attrs[NDA_LLADDR], abort, ERROR, "Error parsing attributes");

    is_local = data->ndm_state == NUD_PERMANENT;
    // Local interfaces currently not supported.
    // Note: we do not only receive messages for ports but also for bridges. By ignoring the
    //   messages with `is_local`, we also ignore those. So if you add support for local interfaces,
    //   you'll have to find a way to identify if a message is for a bridge, so that you can ignore
    //   those messages.
    when_false(!is_local, abort);

    parseresult = calloc(1, sizeof(netdev_parseresult_bridgetable_t));
    when_null_trace(parseresult, abort, ERROR, "Out of mem");

    // get `netdev index`
    when_false_trace(data->ndm_ifindex > 0, abort, ERROR, "Netdevindex negative or zero");
    parseresult->netdev_index = (uint32_t) data->ndm_ifindex;

    // get `mac`
    mac_bin = (unsigned char*) mnl_attr_get_payload(attrs[NDA_LLADDR]);
    mac_bin_len = mnl_attr_get_payload_len(attrs[NDA_LLADDR]);
    mac_str = lladdr2str(bin2lladdr(mac_bin_len, mac_bin));
    when_null_trace(mac_str, abort, ERROR, "Error retrieving mac");
    parseresult->mac = strdup(mac_str);
    when_null_trace(parseresult->mac, abort, ERROR, "Out of mem");

    // get `disappeared`
    parseresult->disappeared = header->nlmsg_type == RTM_DELNEIGH;

    return parseresult;
abort:
    netdev_parseresult_bridgetable_delete(&parseresult);
    return NULL;
}

void netdev_parseresult_bridgetable_delete(netdev_parseresult_bridgetable_t** parseresult) {
    if((parseresult == NULL) || (*parseresult == NULL)) {
        return;
    }

    free((*parseresult)->mac);
    free(*parseresult);
    *parseresult = NULL;
}
