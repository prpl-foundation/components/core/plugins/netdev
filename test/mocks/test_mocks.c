/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "test_mocks.h"

int seq_nr = 0;
int last_msg_was_valid = 0;
int recv_rv[] = {-1, -1};
const char* raw_msg[] = {NULL, NULL};
int next_errno = 0;
int nr_of_msgs = 0;
int next_msg = 0;

static int test_msg_is_valid(const struct msghdr* msg) {
    struct sockaddr_nl* addr = (struct sockaddr_nl*) msg->msg_name;
    int rv = -1;

    when_false(addr->nl_family == AF_NETLINK, exit);
    rv = -2;
    when_false(msg->msg_namelen == sizeof(struct sockaddr_nl), exit);
    rv = -3;
    when_false(msg->msg_iovlen == 1, exit);
    rv = -4;
    when_false(msg->msg_controllen == 0, exit);
    rv = -5;
    when_false(msg->msg_flags == 0, exit);
    rv = -6;
    when_not_null(msg->msg_control, exit);

    rv = 1; // Return a value > 0 to mark that the send_msg was successful

exit:
    last_msg_was_valid = rv;
    return rv;
}

int get_last_sequence_nr(void) {
    return seq_nr;
}

// Returns 0 if no message was send, 1 if the message was valid and a value smaller then 0 if it was invalid
int last_msg_valid(void) {
    int rv = last_msg_was_valid;
    last_msg_was_valid = 0; // Set the value back to -1 to indicate no message was send since this was last read
    return rv;
}

void mock_recv_configure(amxc_var_t* config) {
    int i = 0;

    nr_of_msgs = GET_UINT32(config, "MSGS");
    for(i = 0; (i < nr_of_msgs && i < 2); i++) {
        recv_rv[i] = GETI_INT32(GET_ARG(config, "BUF_LEN"), i);
        raw_msg[i] = GETI_CHAR(GET_ARG(config, "RAW_DATA"), i);
    }
    next_errno = GET_UINT32(config, "ERRNO"); // 0 if not provided
    next_msg = 0;
}

ssize_t __wrap_sendmsg (int __fd, const struct msghdr* __message, UNUSED int __flags) {
    int rv = -1;
    struct nlmsghdr* nlmsghdr = NULL;

    when_false(__fd > 0, exit);

    nlmsghdr = (struct nlmsghdr*) __message->msg_iov->iov_base;
    seq_nr = nlmsghdr->nlmsg_seq;

    rv = test_msg_is_valid(__message);

exit:
    return rv;
}

ssize_t __wrap_recv (int __fd, const void* __buf, UNUSED size_t __n, UNUSED int __flags) {
    int rv = -1;

    if(nr_of_msgs == 1) {
        next_msg = 0;
    }
    if((next_msg >= nr_of_msgs) || (next_msg >= 2)) {
        return -1;
    }

    when_false(__fd > 0, exit);
    when_null(__buf, exit);

    __buf = (void*) raw_msg[next_msg];

    errno = next_errno;
    rv = recv_rv[next_msg];

    next_msg++;


exit:
    return rv;
}
