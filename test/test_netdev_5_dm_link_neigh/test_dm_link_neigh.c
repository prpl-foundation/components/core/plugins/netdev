/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>


#include <arpa/inet.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>


#include "netdev_netlink.h"
#include "dm_link_neigh.h"
#include "dm_link_addr.h"
#include "dm_link.h"
#include "dm_netdev.h"
#include "netdev_util.h"
#include "dm_convert.h"
#include "../common/test_netdev_common_setup.h"
#include "../common/common_functions.h"
#include "test_dm_link_neigh.h"

static link_t* add_test_link(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    link_t* link = NULL;
    amxd_object_t* link_obj = NULL;
    const int index = 48;

    // Msg coming from "ip link add dummy100 type dummy", index = 48
    msg->nlmsg = (struct nlmsghdr*) "\x50\x05\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x30\x00\x00\x00\x82\x00\x00\x00\xff\xff\xff\xff\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x0d\x00\xe8\x03\x00\x00\x05\x00\x10\x00\x02\x00\x00\x00\x05\x00\x11\x00\x00\x00\x00\x00\x08\x00\x04\x00\xdc\x05\x00\x00\x08\x00\x32\x00\x00\x00\x00\x00\x08\x00\x33\x00\x00\x00\x00\x00\x08\x00\x1b\x00\x00\x00\x00\x00\x08\x00\x1e\x00\x00\x00\x00\x00\x08\x00\x1f\x00\x01\x00\x00\x00\x08\x00\x28\x00\xff\xff\x00\x00\x08\x00\x29\x00\x00\x00\x01\x00\x08\x00\x20\x00\x01\x00\x00\x00\x05\x00\x21\x00\x01\x00\x00\x00\x09\x00\x06\x00\x6e\x6f\x6f\x70\x00\x00\x00\x00\x08\x00\x23\x00\x00\x00\x00\x00\x08\x00\x2f\x00\x00\x00\x00\x00\x08\x00\x30\x00\x00\x00\x00\x00\x05\x00\x27\x00\x00\x00\x00\x00\x24\x00\x0e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\x01\x00\x72\x16\x1a\xb4\x68\x0d\x00\x00\x0a\x00\x02\x00\xff\xff\xff\xff\xff\xff\x00\x00\xc4\x00\x17\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x64\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x2b\x00\x05\x00\x02\x00\x00\x00\x00\x00\x10\x00\x12\x00\x0a\x00\x01\x00\x64\x75\x6d\x6d\x79\x00\x00\x00\x0c\x03\x1a\x00\x88\x00\x02\x00\x84\x00\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x02\x0a\x00\x08\x00\x01\x00\x00\x00\x00\x00\x14\x00\x05\x00\xff\xff\x00\x00\x45\xe3\x19\x00\x54\x54\x00\x00\xe8\x03\x00\x00\xe4\x00\x02\x00\x00\x00\x00\x00\x40\x00\x00\x00\xdc\x05\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\xa0\x0f\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x80\x3a\x09\x00\x80\x51\x01\x00\x03\x00\x00\x00\x58\x02\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x60\xea\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\xee\x36\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\xff\xff\x00\x00\xff\xff\xff\xff\x2c\x01\x03\x00\x25\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x34\x00\x06\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x08\x00\x00\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 16;

    // Make sure the link does not already exists
    link = link_find(index);
    assert_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_null(link_obj);

    // Call the handler and handle events so the link exist in the DM as well
    assert_int_equal(handleNewLink(msg), 0);
    handle_events();

    link = link_find(48);
    assert_non_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(link_obj);

    return link;
}

static neigh_t* add_test_neigh(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    neigh_t ws;
    neigh_t* neigh = NULL;
    amxd_object_t* neigh_obj = NULL;

    //Message coming from the msg generated by "ip neighbor add 192.168.200.1 lladdr 00:c0:7b:7d:00:c8 dev dummy100 nud permanent router"
    msg->nlmsg = (struct nlmsghdr*) "\x4c\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x53\x74\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x80\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x0a\x00\x02\x00\x00\xc0\x7b\x7d\x00\xc8\x00\x00\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the neighs does not yet exist
    assert_int_equal(msg2neigh(&ws, msg), 0);
    neigh = neigh_find(&ws);
    assert_null(neigh);

    // Call handler to add the neighs
    assert_int_equal(handleNewNeigh(msg), 0);
    handle_events();

    neigh = neigh_find(&ws);
    assert_non_null(neigh);
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", neigh->dyn_index);
    assert_non_null(neigh_obj);

    return neigh;
}

int test_netdev_setup(void** state) {
    test_netdev_common_setup(state, false);
    assert_int_equal(0, amxo_parser_scan_mib_dir(test_setup_parser(), "../../mibs"));
    _netdev_main(AMXO_START, test_setup_dm(), test_setup_parser());
    handle_events();

    return 0;
}

int test_netdev_teardown(void** state) {
    _netdev_main(AMXO_STOP, test_setup_dm(), test_setup_parser());
    handle_events();
    test_netdev_common_teardown(state);
    return 0;
}

void test_init_neigh_family_converter(UNUSED void** state) {
    converter_t* test_converter = init_neigh_family_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, AF_INET), "ipv4");
    assert_string_equal(convert_bin2str(test_converter, AF_INET6), "ipv6");
    assert_string_equal(convert_bin2str(test_converter, AF_BRIDGE), "bridge");

    // Any other bin value should return an empty string because the converter attribute is set to non
    assert_string_equal(convert_bin2str(test_converter, 99), "");
    assert_string_equal(convert_bin2str(test_converter, 35), "");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, AF_INET), "");
}

void test_init_neigh_flags_converter(UNUSED void** state) {
    converter_t* test_converter = init_neigh_flags_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, NTF_PROXY), "proxy");
    assert_string_equal(convert_bin2str(test_converter, NTF_ROUTER), "router");

    // For converter_attribute_flags it should be possible to get a combination of flags
    assert_string_equal(convert_bin2str(test_converter, NTF_PROXY | NTF_ROUTER), "proxy router");

    // Any other bin value should return an empty string
    assert_string_equal(convert_bin2str(test_converter, 65536), "");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, NTF_PROXY), "");
}

void test_init_neigh_state_converter(UNUSED void** state) {
    converter_t* test_converter = init_neigh_state_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, NUD_INCOMPLETE), "incomplete");
    assert_string_equal(convert_bin2str(test_converter, NUD_REACHABLE), "reachable");
    assert_string_equal(convert_bin2str(test_converter, NUD_STALE), "stale");
    assert_string_equal(convert_bin2str(test_converter, NUD_DELAY), "delay");
    assert_string_equal(convert_bin2str(test_converter, NUD_PROBE), "probe");
    assert_string_equal(convert_bin2str(test_converter, NUD_FAILED), "failed");
    assert_string_equal(convert_bin2str(test_converter, NUD_NOARP), "noarp");
    assert_string_equal(convert_bin2str(test_converter, NUD_PERMANENT), "permanent");

    // For converter_attribute_flags it should be possible to get a combination of flags
    assert_string_equal(convert_bin2str(test_converter, NUD_INCOMPLETE | NUD_REACHABLE |
                                        NUD_STALE | NUD_DELAY | NUD_PROBE | NUD_FAILED |
                                        NUD_NOARP | NUD_PERMANENT),
                        "incomplete reachable stale delay probe failed noarp permanent");

    // Any other bin value should return an empty string
    assert_string_equal(convert_bin2str(test_converter, 65536), "");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, NUD_STALE), "");
}

/*
 * Test the neigh_copy function
 * Expectations:
 *  - When providing invalid input the function should return -1
 *  - When valid inputs are provided the destination neigh should be a copy of the source*
 *      * with exception of index, dyn_index and object
 */
void test_neigh_copy(UNUSED void** state) {
    neigh_t neigh_src;
    neigh_t neigh_dst;

    memset(&neigh_src, 0, sizeof(neigh_t));
    memset(&neigh_dst, 0, sizeof(neigh_t));

    // Values that are not copied
    neigh_src.active = true;
    neigh_src.dyn_index = 5;
    neigh_src.index = 1;
    // Build up valid input
    neigh_src.family = AF_INET;
    neigh_src.flags = NTF_ROUTER;
    neigh_src.link = 48;
    neigh_src.state = NUD_REACHABLE | NUD_PERMANENT;
    memcpy(neigh_src.dst, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    lladdr_copy(&neigh_src.lladdr, str2lladdr("60:30:15:AA:BB:CC", NULL));

    // First provide invalid input
    assert_int_equal(neigh_copy(&neigh_dst, NULL), -1);
    assert_int_equal(neigh_copy(NULL, &neigh_src), -1);

    // Provide valid input
    assert_int_equal(neigh_copy(&neigh_dst, &neigh_src), 0);

    // Check if copy was successful
    assert_int_equal(neigh_dst.family, neigh_src.family);
    assert_int_equal(neigh_dst.flags, neigh_src.flags);
    assert_int_equal(neigh_dst.link, neigh_src.link);
    assert_int_equal(neigh_dst.state, neigh_src.state);
    assert_string_equal(addr2str(neigh_dst.family, neigh_dst.dst, false), "192.168.0.1");
    assert_string_equal(lladdr2str(&neigh_dst.lladdr), "60:30:15:AA:BB:CC");

    assert_false(neigh_dst.active);
    assert_int_equal(neigh_dst.dyn_index, 0);
    assert_int_equal(neigh_dst.index, 0);

}

/*
 * Test the functions to create, find and destroy the neigh structs
 * Expectations:
 *  - When an neigh is created it should be active
 *  - When the correct information is provided it should be able be found
 *  - The found neigh should contain the same information
 *  - When the neigh_destroy is called it should be scheduled for deletion and no longer be able to be found
 */
void test_neigh_struct_functions(UNUSED void** state) {
    neigh_t* found_neigh = NULL;
    neigh_t* neigh = neigh_create();
    neigh_t ws;

    memset(&ws, 0, sizeof(neigh_t));
    assert_true(neigh->active);

    // Set the required values
    neigh->family = AF_INET;
    neigh->link = 48;
    neigh->flags = NTF_ROUTER;
    memcpy(neigh->dst, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));

    // Copy (only) the values required to find the neigh to the ws
    neigh_copy(&ws, neigh);

    // Set the optional values
    neigh->dyn_index = 5;
    neigh->index = 1;
    neigh->state = NUD_REACHABLE | NUD_PERMANENT;
    lladdr_copy(&neigh->lladdr, str2lladdr("60:30:15:AA:BB:CC", NULL));

    found_neigh = neigh_find(NULL);
    assert_null(found_neigh);
    found_neigh = neigh_find(&ws);
    assert_non_null(found_neigh);

    // Check if all parameters in the found neigh match with the one we where looking for
    assert_int_equal(found_neigh->family, AF_INET);
    assert_int_equal(found_neigh->link, 48);
    assert_int_equal(found_neigh->flags, NTF_ROUTER);
    assert_string_equal(addr2str(found_neigh->family, found_neigh->dst, false), "192.168.0.1");
    assert_int_equal(found_neigh->dyn_index, 5);
    assert_int_equal(found_neigh->index, 1);
    assert_int_equal(found_neigh->state, NUD_REACHABLE | NUD_PERMANENT);
    assert_string_equal(lladdr2str(&found_neigh->lladdr), "60:30:15:AA:BB:CC");

    ws.family = AF_INET6;
    found_neigh = neigh_find(&ws);
    assert_null(found_neigh);

    ws.family = AF_INET;
    ws.link = 23;
    found_neigh = neigh_find(&ws);
    assert_null(found_neigh);

    ws.link = 48;
    ws.flags = NTF_MASTER;
    found_neigh = neigh_find(&ws);
    assert_non_null(found_neigh); // When only setting a different flag we should get a result

    ws.flags = NTF_ROUTER;
    memcpy(neigh->dst, str2addr(AF_INET, "192.168.10.1", NULL), addrlen(AF_INET));
    found_neigh = neigh_find(&ws);
    assert_null(found_neigh);

    neigh_destroy(neigh, false);
    assert_false(neigh->active);
    found_neigh = neigh_find(&ws);
    assert_null(found_neigh);

    handle_events();
}


/*
 * Make sure msg2neigh can handle invalid inputs
 * Expectations:
 *  - Invalid inputs return a -1 instead of causing a valgrind error
 */
void test_msg2neigh_invalid_input(UNUSED void** state) {
    neigh_t neigh;
    rtnlmsg_t* msg = rtnlmsg_instance();

    assert_int_equal(msg2neigh(NULL, msg), -1);
    msg->nlmsg = NULL;
    assert_int_equal(msg2neigh(&neigh, msg), -1);
    msg = NULL;
    assert_int_equal(msg2neigh(&neigh, msg), -1);
}


/*
 * Make sure that netlink messages are converted to the neigh structure correctly
 * The test message comes from calling "ip a add 192.168.100.1/24 dev dummy100" and assumes that the dummy100 link is link 48
 * Expectations:
 *  - All parameters present in the netlink msg are set in the neigh struct
 */
void test_msg2neigh(UNUSED void** state) {
    neigh_t neigh;
    rtnlmsg_t* msg = rtnlmsg_instance();

    //Message coming from the msg generated by "ip neighbor add 192.168.200.1 lladdr 00:c0:7b:7d:00:c8 dev dummy100 nud permanent router"
    msg->nlmsg = (struct nlmsghdr*) "\x4c\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x8a\x55\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x80\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x0a\x00\x02\x00\x00\xc0\x7b\x7d\x00\xc8\x00\x00\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    assert_int_equal(msg2neigh(&neigh, msg), 0);

    // parameters not set by the msg2neigh should be 0
    assert_int_equal(neigh.dyn_index, 0);
    assert_int_equal(neigh.index, 0);

    // Other parameters
    assert_int_equal(neigh.family, AF_INET);
    assert_int_equal(neigh.link, 48);
    assert_int_equal(neigh.flags, NTF_ROUTER);
    assert_string_equal(addr2str(neigh.family, neigh.dst, false), "192.168.200.1");
    assert_int_equal(neigh.state, NUD_PERMANENT);
    assert_string_equal(lladdr2str(&neigh.lladdr), "00:C0:7B:7D:00:C8");
}

/*
 * Make sure the translation from neigh to transaction works properly
 * Expectations:
 *  - When provided with invalid inputs the function should fail
 *  - When provided with valid inputs the transactions should contain all values
 *      - When the converters are not initialized some values will be an empty string
 *      - When the converters are initialized the values should be correctly converted
 */
void test_create_transaction_from_neigh(UNUSED void** state) {
    int rv = -1;
    neigh_t* neigh = neigh_create();
    amxd_trans_t* trans = NULL;
    amxc_var_t* expected_data = NULL;

    // Build up valid neigh
    neigh->active = true;
    neigh->dyn_index = 10;
    neigh->index = 1;
    neigh->family = AF_INET;
    neigh->flags = NTF_ROUTER;
    neigh->link = 100;
    neigh->state = NUD_REACHABLE | NUD_PERMANENT;
    memcpy(neigh->dst, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    lladdr_copy(&neigh->lladdr, str2lladdr("60:30:15:AA:BB:CC", NULL));

    // Call with invalid input
    trans = create_transaction_from_neigh(NULL, true);
    assert_null(trans);
    trans = create_transaction_from_neigh(NULL, false);
    assert_null(trans);

    // Call with valid input
    expected_data = read_json_from_file("test_data/trans_new_neigh_no_converters.json");
    trans = create_transaction_from_neigh(neigh, true);
    rv = validate_transaction(trans, "NetDev.Link.100.Neigh.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_neigh_no_converters.json");
    trans = create_transaction_from_neigh(neigh, false);
    rv = validate_transaction(trans, "NetDev.Link.100.Neigh.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    neigh_init_converters();

    expected_data = read_json_from_file("test_data/trans_new_neigh.json");
    trans = create_transaction_from_neigh(neigh, true);
    rv = validate_transaction(trans, "NetDev.Link.100.Neigh.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_neigh.json");
    trans = create_transaction_from_neigh(neigh, false);
    rv = validate_transaction(trans, "NetDev.Link.100.Neigh.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    neigh_cleanup();
}

/*
 * Provide the handleNewNeigh function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleNewNeigh_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();

    msg->nlmsg = NULL;
    assert_int_equal(handleNewNeigh(msg), -1);
    msg = NULL;
    assert_int_equal(handleNewNeigh(msg), -1);
}


/*
 * Provides the handleNewNeigh with an invalid scenario
 *  - The neigh test data is coming from the neigh created by doing "ip a add 192.168.100.1/24 dev dummy100" manually and storing the msg in this test
 *  - No valid link exist to which to add the neigh
 * Expectations:
 *  - Before calling the handleNewNeigh no matching neigh should exist
 *  - handleNewNeigh should return with issue
 *  - An active neigh struct should be found directly after calling the handleNewNeigh (no dm instance yet)
 *  - After handling the events
 *      - The neigh structure should no longer be accessed since it should be invalid
 *      - The neigh should no longer be found by the neigh_find function
 *      - No neigh instance should exist in the datamodel
 */
void test_handleNewNeigh_link_does_not_exist(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    neigh_t ws;
    neigh_t* neigh = NULL;
    amxd_object_t* neigh_obj = NULL;
    int dyn_index = 0;

    //Message coming from the msg generated by "ip neighbor add 192.168.200.1 lladdr 00:c0:7b:7d:00:c8 dev dummy100 nud permanent router"
    msg->nlmsg = (struct nlmsghdr*) "\x4c\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x8a\x55\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x80\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x0a\x00\x02\x00\x00\xc0\x7b\x7d\x00\xc8\x00\x00\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the neigh does not yet exist
    assert_int_equal(msg2neigh(&ws, msg), 0);
    neigh = neigh_find(&ws);
    assert_null(neigh);

    // Call handler for neigh where the link does not exist
    assert_int_equal(handleNewNeigh(msg), 0); // handle function should be successful even if the link does not exist

    // The neigh should exist as soon as the handler is called
    neigh = neigh_find(&ws);
    assert_non_null(neigh);
    assert_true(neigh->active);
    // Should not exist in the DM until events are handled
    dyn_index = neigh->dyn_index; // Save this so it can be used after handling events since we expect neigh to be removed then
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", neigh->dyn_index);
    assert_null(neigh_obj);

    handle_events();
    // The neigh struct should be removed since the transaction should fail
    neigh = neigh_find(&ws);
    assert_null(neigh);
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_null(neigh_obj);
}


/*
 * Provides the handleNewNeigh with a valid scenario
 *  - A valid dummy link is provided by calling the add_test_link function
 *  - The neigh test data is coming from the neigh created by manually addind neighs in docker and storing the msg in this test
 * Expectations:
 *  - The test_link is added without a problem (this must be done before storing the neigh msg otherwise the add_test_link function will overwrite it)
 *  - Before calling the handleNewNeigh no matching neigh should exist
 *  - handleNewNeigh should return without issue
 *  - An active neigh struct should be found directly after calling the handleNewNeigh (no dm instance yet)
 *  - After handling the events
 *      - The neigh struct should still be active
 *      - An neigh instance should exist in the datamodel
 *      - The data should match
 *  - When calling the handleNewNeigh for the second time (update message)
 *      - The neigh struct should be updated
 *      - The existing dm instance should be updated
 *  - When calling neigh_destroy with the option set to true
 *      - Right after calling:
 *          - The neigh should become inactive
 *          - The neigh should no longer be found by the neigh_find function
 *          - The neigh instance should remain in the DM
 *      - After handling the events:
 *          - The neigh structure should no longer be accessed since it will be invalid
 *          - The neigh instance should be removed from the DM
 */
void test_handleNewNeigh(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    neigh_t ws;
    neigh_t* neigh = NULL;
    link_t* test_link = NULL;
    amxd_object_t* neigh_obj = NULL;
    int dyn_index = 0;
    amxc_var_t params;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    //Message coming from the msg generated by "ip neighbor add 192.168.200.1 lladdr 00:c0:7b:7d:00:c8 dev dummy100 nud permanent router"
    msg->nlmsg = (struct nlmsghdr*) "\x4c\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x8a\x55\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x80\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x0a\x00\x02\x00\x00\xc0\x7b\x7d\x00\xc8\x00\x00\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00";

    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the neigh does not yet exist
    assert_int_equal(msg2neigh(&ws, msg), 0);
    neigh = neigh_find(&ws);
    assert_null(neigh);

    // Call handler to add the new neighs
    assert_int_equal(handleNewNeigh(msg), 0);

    // The neigh should exist as soon as the handler is called
    neigh = neigh_find(&ws);
    assert_non_null(neigh);
    assert_true(neigh->active);
    // Should not exist in the DM until events are handled
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", neigh->dyn_index);
    assert_null(neigh_obj);

    handle_events();
    neigh = neigh_find(&ws);
    assert_non_null(neigh);
    assert_true(neigh->active);
    assert_int_equal(neigh->family, AF_INET);
    assert_int_equal(neigh->link, 48);
    assert_int_equal(neigh->flags, NTF_ROUTER);
    assert_string_equal(addr2str(neigh->family, neigh->dst, false), "192.168.200.1");
    assert_int_equal(neigh->state, NUD_PERMANENT);
    assert_string_equal(lladdr2str(&neigh->lladdr), "00:C0:7B:7D:00:C8");
    dyn_index = neigh->dyn_index;
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", neigh->dyn_index);
    assert_non_null(neigh_obj);

    assert_int_equal(neigh->index, neigh_obj->index);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(neigh_obj, &params, amxd_dm_access_protected);

    assert_true(neigh_obj == neigh->object);
    assert_string_equal(GET_CHAR(&params, "Dst"), "192.168.200.1");
    assert_string_equal(GET_CHAR(&params, "Family"), "ipv4");
    assert_string_equal(GET_CHAR(&params, "Flags"), "router");
    assert_string_equal(GET_CHAR(&params, "LLAddress"), "00:C0:7B:7D:00:C8");
    assert_string_equal(GET_CHAR(&params, "State"), "permanent");
    amxc_var_clean(&params);

    // Update state from permanent to reachable
    msg->nlmsg = (struct nlmsghdr*) "\x4c\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x8a\x55\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x02\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x0a\x00\x02\x00\x00\xc0\x7b\x7d\x00\xc8\x00\x00\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00";

    assert_int_equal(handleNewNeigh(msg), 0);
    handle_events();

    neigh = neigh_find(&ws);
    assert_non_null(neigh);
    assert_true(neigh->active);
    assert_non_null(neigh->object);
    assert_int_equal(neigh->state, NUD_REACHABLE); // No flags set
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", neigh->dyn_index);
    assert_non_null(neigh_obj);
    assert_non_null(neigh_obj->priv);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(neigh_obj, &params, amxd_dm_access_protected);

    assert_true(neigh_obj == neigh->object);
    assert_true(neigh_obj == neigh->object);
    assert_string_equal(GET_CHAR(&params, "Dst"), "192.168.200.1");
    assert_string_equal(GET_CHAR(&params, "Family"), "ipv4");
    assert_string_equal(GET_CHAR(&params, "Flags"), "router");
    assert_string_equal(GET_CHAR(&params, "LLAddress"), "00:C0:7B:7D:00:C8");
    assert_string_equal(GET_CHAR(&params, "State"), "reachable");
    amxc_var_clean(&params);

    neigh_destroy(neigh, true);
    assert_false(neigh->active);
    assert_null(neigh->object);
    neigh = neigh_find(&ws);
    assert_null(neigh);
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_non_null(neigh_obj); // Should only be removed from the datamodel after events are handled

    handle_events();

    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_null(neigh_obj);
}

/*
 * Provide the handleDelNeigh function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleDelNeigh_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    assert_int_equal(handleDelNeigh(NULL), -1);
    msg->nlmsg = NULL;
    assert_int_equal(handleDelNeigh(msg), -1);
}


/*
 * Make sure the delete handler does not schedule neighs to be deleted that are already inactive
 * Expectations:
 *  - After calling the delete handler (expected to fail because no active niegh could be found)
 *    - The instance should still exist in the DM
 *  - After handling the events
 *    - The object value in the neigh struct should NOT be set to NULL
 *    - The datamodel objects priv value should NOT be set to NULL
 *    - The instance should still be available in the DM
 */
void test_handleDelNeigh_inactive_neigh(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    neigh_t* test_neigh = NULL;
    link_t* test_link = NULL;
    amxd_object_t* neigh_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_neigh = add_test_neigh();
    assert_non_null(test_neigh);
    dyn_index = test_neigh->dyn_index;

    test_neigh->active = false;

    // Call the handler to remove the neigh
    msg->nlmsg = (struct nlmsghdr*) "\x40\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x06\x00\x00\x00\x20\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\x44\x2d\x00\x00\x44\x2d\x00\x00\x44\x2d\x00\x00\x00\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;
    assert_int_equal(handleDelNeigh(msg), -1); // Should fail because no active neigh was found
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_non_null(neigh_obj);
    assert_non_null(neigh_obj->priv);
    assert_non_null(test_neigh->object);

    handle_events();
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_non_null(neigh_obj);
    assert_non_null(neigh_obj->priv);
    assert_non_null(test_neigh->object);

    test_neigh->active = true;
    neigh_destroy(test_neigh, true);
    handle_events();
}

/*
 * Make sure the neigh delete messages are handled correctly
 * Expectations:
 *  - Directly after calling the handleDelNeigh
 *    - The neigh should go to inactive
 *    - The object value in the neigh struct should be set to NULL
 *    - The datamodel objects priv value should be set to NULL
 *  - After handling the events, the neigh should be removed from the dm.
 */
void test_handleDelNeigh(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    neigh_t* neigh = NULL;
    neigh_t* test_neigh = NULL;
    link_t* test_link = NULL;
    amxd_object_t* neigh_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_neigh = add_test_neigh();
    assert_non_null(test_neigh);
    dyn_index = test_neigh->dyn_index;

    // Call the handler to remove the neigh
    msg->nlmsg = (struct nlmsghdr*) "\x40\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x54\x74\x00\x00\x02\x00\x00\x00\x30\x00\x00\x00\x20\x00\x80\x01\x08\x00\x01\x00\xc0\xa8\xc8\x01\x08\x00\x04\x00\x00\x00\x00\x00\x14\x00\x03\x00\xc0\x02\x00\x00\xc0\x02\x00\x00\xc0\x02\x00\x00\x01\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;
    assert_int_equal(handleDelNeigh(msg), 0);
    assert_false(test_neigh->active);
    assert_null(test_neigh->object);
    neigh = neigh_find(test_neigh);
    assert_null(neigh);

    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_non_null(neigh_obj);
    assert_null(neigh_obj->priv);

    handle_events();
    neigh_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.Neigh.dyn%d", dyn_index);
    assert_null(neigh_obj);
}
