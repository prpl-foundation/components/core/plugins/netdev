/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <arpa/inet.h>
#include <linux/rtnetlink.h>

#include "netdev_netlink.h"
#include "../common/common_functions.h"
#include "test_netdev_nl.h"
#include "test_mocks.h"

uint32_t handler_calls = 0;

static void init_rtnl_stats(amxc_var_t* rtnl_stats) {
    amxc_var_init(rtnl_stats);
    amxc_var_set_type(rtnl_stats, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsError", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsErrorLost", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsErrorResend", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsHandled", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsMultiPartDone", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsNoOp", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsPartial", 0);
    amxc_var_add_key(uint32_t, rtnl_stats, "NetLinkMsgsTotal", 0);
}

static int dummy_handler_fn(UNUSED rtnlmsg_t* msg) {
    handler_calls++;
    return 0;
}

static void dummy_print_fn(UNUSED FILE* f, UNUSED rtnlmsg_t* msg) {
    // Function is only for dummy purposes
}

/**
 * used to make testing the rtnl_read function easier
 * @param msg initialized rtnl message
 * @param success Should be set to true if rtnl_read is expected to be successful
 * @param expect_read Should be set to true if this call is expected to read the provided message
 * @param expect_handler_call Should be set to true if this call is expected to call the 'dummy_handler_fn' handler
 * @param blocking This value is directly passed to the rtnl_read function blocking parameter
 * @return 0 if ok, -1 in case of unknown error, positive value in case of test failure
 */
static int test_helper_rtnl_read(rtnlmsg_t* msg, bool success, bool expect_read, bool expect_handler_call, bool blocking) {
    int rv = -1;
    static uint32_t msgs_read = 0;
    static uint32_t expected_handler_calls = 0;
    amxc_var_t rtnl_stats;
    init_rtnl_stats(&rtnl_stats);

    when_false_status(rtnl_read(1, msg, blocking) == success, exit, rv = 1);
    if(expect_read) {
        msgs_read++;
    }
    if(expect_handler_call) {
        expected_handler_calls++;
    }
    get_rtnl_read_stats(&rtnl_stats);
    when_false_status((GET_UINT32(&rtnl_stats, "NetLinkMsgsTotal") == msgs_read), exit, rv = 2);
    when_false_status((GET_UINT32(&rtnl_stats, "NetLinkMsgsHandled") == handler_calls), exit, rv = 3);
    when_false_status((handler_calls == expected_handler_calls), exit, rv = 4);
    rv = 0;

exit:
    amxc_var_clean(&rtnl_stats);
    return rv;
}

int test_netdev_setup(UNUSED void** state) {
    return 0;
}

int test_netdev_teardown(UNUSED void** state) {
    return 0;
}

void test_rtnlmsg_history(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    int i = 0;
    int max_history_len = 16;
    int overflow = 4;

    rtnlmsg_initialize(msg, RTM_GETADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);

    // Fill the history buffer with msgs
    for(i = 1; i <= max_history_len; i++) {
        msg->nlmsg->nlmsg_seq = i;
        msg->hdrlen = i;
        assert_true(rtnlmsg_store(msg));
    }

    // Check if all msgs can be fetched again
    for(i = 1; i <= max_history_len; i++) {
        rtnlmsg_t* rtnlmsg = NULL;
        msg->nlmsg->nlmsg_seq = i;
        rtnlmsg = rtnlmsg_load(msg->nlmsg);
        assert_non_null(rtnlmsg);
        assert_int_equal(rtnlmsg->hdrlen, i);
        assert_int_equal(rtnlmsg->nlmsg->nlmsg_seq, i);
    }

    // Add additional message so the buffer has to remove the oldest messages
    for(i = max_history_len + 1; i <= max_history_len + overflow; i++) {
        msg->nlmsg->nlmsg_seq = i;
        assert_true(rtnlmsg_store(msg));
    }

    // Check if the first messages can not longer be fetched
    for(i = 1; i <= overflow; i++) {
        rtnlmsg_t* rtnlmsg = NULL;
        msg->nlmsg->nlmsg_seq = i;
        rtnlmsg = rtnlmsg_load(msg->nlmsg);
        assert_null(rtnlmsg);
    }

    // Check if newer msgs can still be fetched again
    for(i = overflow + 1; i <= max_history_len; i++) {
        rtnlmsg_t* rtnlmsg = NULL;
        msg->nlmsg->nlmsg_seq = i;
        rtnlmsg = rtnlmsg_load(msg->nlmsg);
        assert_non_null(rtnlmsg);
        assert_int_equal(rtnlmsg->hdrlen, i);
        assert_int_equal(rtnlmsg->nlmsg->nlmsg_seq, i);
    }

    // Test if invalid input is handled
    assert_false(rtnlmsg_store(NULL));
    msg->nlmsg = NULL;
    assert_false(rtnlmsg_store(msg));
    msg = NULL;
    assert_false(rtnlmsg_store(msg));

    assert_null(rtnlmsg_load(NULL));

}

void test_rtnl_register_handler(UNUSED void** state) {
    rtnlhandler_t testHandler = {RTM_NEWROUTE, sizeof(struct rtmsg), dummy_handler_fn, dummy_print_fn};
    rtnlhandler_t testHandler2 = {0, 0, NULL, NULL};

    assert_int_equal(rtnl_register_handler(&testHandler), 0);
    assert_int_equal(rtnl_register_handler(&testHandler2), 0);
    assert_int_equal(rtnl_register_handler(NULL), -1);

    rtnl_unregister_handlers();
}

void test_rtnl_send(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_t* msg2 = rtnlmsg_instance();
    int seq_start = get_last_sequence_nr();

    rtnlmsg_initialize(msg, RTM_GETADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
    assert_true(rtnl_send(1, msg));
    // Check if sequence number is correctly increased
    assert_int_equal(get_last_sequence_nr(), seq_start + 1);
    // Check if sequence number is set in the nlmsg
    assert_int_equal(msg->nlmsg->nlmsg_seq, get_last_sequence_nr());
    // Check if the message contained all data that was expected
    assert_int_equal(last_msg_valid(), 1);
    // Make sure the massage was saved in the history
    assert_non_null(rtnlmsg_load(msg->nlmsg));

    rtnlmsg_initialize(msg2, RTM_GETLINK, sizeof(struct ifaddrmsg), NLM_F_DUMP);
    assert_true(rtnl_send(1, msg2));
    // Check if sequence number is correctly increased
    assert_int_equal(get_last_sequence_nr(), seq_start + 2);
    // Check if sequence number is set in the nlmsg
    assert_int_equal(msg2->nlmsg->nlmsg_seq, get_last_sequence_nr());
    // Check if the message contained all data that was expected
    assert_int_equal(last_msg_valid(), 1);
    // Make sure the massage was saved in the history
    assert_non_null(rtnlmsg_load(msg2->nlmsg));

    // Resave the sequence number to the start of the failing tests
    seq_start = get_last_sequence_nr();
    // Check function fails when invalid file descriptor is given
    assert_false(rtnl_send(-1, msg));
    // Sequence number should not increase
    assert_int_equal(get_last_sequence_nr(), seq_start);
    // Check if no (valid) message was send
    assert_int_equal(last_msg_valid(), 0);

    msg->nlmsg = NULL;
    assert_false(rtnl_send(1, msg));
    // Sequence number should not increase
    assert_int_equal(get_last_sequence_nr(), seq_start);
    // Check if no (valid) message was send
    assert_int_equal(last_msg_valid(), 0);

    assert_false(rtnl_send(1, NULL));
    // Sequence number should not increase
    assert_int_equal(get_last_sequence_nr(), seq_start);
    // Check if no (valid) message was send
    assert_int_equal(last_msg_valid(), 0);

}

void test_rtnl_read_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_NEWLINK, sizeof(struct ifaddrmsg), NLM_F_REQUEST);

    // Test invalid fds
    assert_false(rtnl_read(-1, msg, true));
    assert_false(rtnl_read(-1, msg, false));
    assert_false(rtnl_read(0, msg, true));
    assert_false(rtnl_read(0, msg, false));
    // Test invalid msgs
    msg->nlmsg = NULL;
    assert_false(rtnl_read(1, msg, true));
    assert_false(rtnl_read(1, msg, false));
    msg = NULL;
    assert_false(rtnl_read(1, msg, true));
    assert_false(rtnl_read(1, msg, false));
}

// This test checks if the rtnl_read function handles the cases where recv returns a value <= 0
void test_rtnl_read_invalid_buf_len(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    amxc_var_t* data = NULL;
    static rtnlhandler_t newLinkHandler = { RTM_NEWLINK, sizeof(struct ifinfomsg), dummy_handler_fn, dummy_print_fn};

    rtnlmsg_initialize(msg, RTM_NEWLINK, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
    // Add the correct handler
    rtnl_register_handler(&newLinkHandler);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_negative_buf_len.json");
    mock_recv_configure(data);

    // Call message with invalid return, expect failure
    assert_int_equal(test_helper_rtnl_read(msg, false, false, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, false, false, false, false), 0);
    amxc_var_delete(&data);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_negative_buf_len_errno_4.json");
    mock_recv_configure(data);

    // Call message with invalid return, but errno EINTR, should not fail but no message should be read
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, false), 0);
    amxc_var_delete(&data);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_negative_buf_len_errno_11.json");
    mock_recv_configure(data);

    // Call message with invalid return, but errno EAGAIN, should not fail but no message should be read
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, false), 0);
    amxc_var_delete(&data);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_negative_buf_len_errno_105.json");
    mock_recv_configure(data);

    // Call message with invalid return, but errno ENOBUFS, should not fail but no message should be read
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, false, false, false), 0);
    amxc_var_delete(&data);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_zero_buf_len.json");
    mock_recv_configure(data);

    // Call message with invalid return, expect failure
    assert_int_equal(test_helper_rtnl_read(msg, false, false, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, false, false, false, false), 0);
    amxc_var_delete(&data);

    rtnl_unregister_handlers();
}

void test_rtnl_read(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    amxc_var_t* data = NULL;

    static rtnlhandler_t newLinkHandler = { RTM_NEWLINK, sizeof(struct ifinfomsg), dummy_handler_fn, dummy_print_fn};
    static rtnlhandler_t newRouteHandler = { RTM_NEWROUTE, sizeof(struct ifinfomsg), dummy_handler_fn, dummy_print_fn};

    rtnlmsg_initialize(msg, RTM_NEWLINK, sizeof(struct ifaddrmsg), NLM_F_REQUEST);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_link_add.json");
    mock_recv_configure(data);

    // Call message without any handlers configured
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, false), 0);

    // Add handler for a different message type
    rtnl_register_handler(&newRouteHandler);
    // Call message with wrong handler configured
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, false), 0);

    // Add the correct handler
    rtnl_register_handler(&newLinkHandler);
    // Call message with handler
    assert_int_equal(test_helper_rtnl_read(msg, true, true, true, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, true, true, false), 0);

    amxc_var_delete(&data);
    rtnl_unregister_handlers();
}

// This test is disabled because of issue with __wrap_recv not returning the second message properly
void test_rtnl_read_multipart(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    amxc_var_t* data = NULL;

    rtnlmsg_initialize(msg, RTM_NEWNEIGH, sizeof(struct ifaddrmsg), NLM_F_MULTI);

    // Prepare the message to be read
    data = read_json_from_file("test_data/test_multipart.json");
    mock_recv_configure(data);

    // Call message without any handlers configured
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, true), 0);
    assert_int_equal(test_helper_rtnl_read(msg, true, true, false, false), 0);

    amxc_var_delete(&data);
}