/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_netdev_common_setup.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/dummy_backend.h"

#include <debug/sahtrace.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#include <amxb/amxb.h>
#include <amxc/amxc_macros.h>

#include "common_functions.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define DUMMY_TEST_ODL "../common/dummy.odl"

amxo_parser_t* test_setup_parser(void) {
    return &parser;
}

amxd_dm_t* test_setup_dm(void) {
    return &dm;
}

amxb_bus_ctx_t* test_setup_bus_ctx(void) {
    assert_non_null(bus_ctx);
    return bus_ctx;
}

void test_setup_destroy_bus_ctx(void) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(UNUSED const char* object_path) {
    return test_setup_bus_ctx();
}

void test_setup_parse_odl(const char* odl_file_name) {
    amxd_object_t* root_obj = amxd_dm_get_root(test_setup_dm());
    if(0 != amxo_parser_parse_file(test_setup_parser(), odl_file_name, root_obj)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&test_setup_parser()->msg, 0));
    }
}

int test_netdev_common_setup(UNUSED void** state, bool enable_tracing) {
    // Enable error printing. Only has effect if you also enable it at compile time:
    //   make clean && CFLAGS="-DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500" make -C test
    if(enable_tracing) {
        sahTraceOpen("test", TRACE_TYPE_STDERR);
        sahTraceSetLevel(TRACE_LEVEL_ERROR);
        sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    }

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);

    resolver_add_all_functions(&parser);

    test_setup_parse_odl("../../odl/netdev_definitions.odl");
    test_setup_parse_odl("testdata.odl");

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    handle_events();

    return 0;
}

int test_netdev_common_teardown(UNUSED void** state) {
    test_setup_destroy_bus_ctx();

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    sahTraceClose();

    return 0;
}
