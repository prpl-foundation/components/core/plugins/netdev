/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_util.h"
#include "test_netdev_common_setup.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/dummy_backend.h"

#include <debug/sahtrace.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#include <amxb/amxb.h>
#include <amxc/amxc_macros.h>

/** note: also works for name-based paths, not just index-based */
static amxc_var_t* s_get_param(const char* path, const char* param_name) {
    amxc_var_t params_with_path;
    amxc_var_t* params = NULL;
    amxc_var_t* value = NULL;
    amxc_var_t* value_copy = NULL;
    int rv = 0;
    amxc_var_init(&params_with_path);

    rv = amxb_get(test_setup_bus_ctx(), path, 0, &params_with_path, 1);
    if(rv != 0) {
        fail_msg("Cannot find '%s'", path);
    }
    params = amxc_var_get_first(amxc_var_get_first(&params_with_path));
    value = GET_ARG(params, param_name);
    if(value == NULL) {
        fail_msg("Cannot find parameter '%s' in '%s'", param_name, path);
    }
    amxc_var_new(&value_copy);
    amxc_var_copy(value_copy, value);
    amxc_var_clean(&params_with_path);

    return value_copy;
}

void assert_dm_str(const char* path, const char* parameter, const char* expected_value) {
    amxc_var_t* var = s_get_param(path, parameter);
    if(AMXC_VAR_ID_CSTRING != amxc_var_type_of(var)) {
        fail_msg("Unexpected type for %s.%s", path, parameter);
    }
    const char* actual_value = amxc_var_constcast(cstring_t, var);
    assert_string_equal(expected_value, actual_value);
    amxc_var_delete(&var);
}
