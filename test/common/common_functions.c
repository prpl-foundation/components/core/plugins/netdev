/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "common_functions.h"

#include "dm_link.h"
#include "dm_netdev.h"
#include "dm_link_addr.h"
#include "dm_link_route.h"
#include "dm_link_neigh.h"
#include "dm_link_stats.h"
#include "netdev_netlink.h"
#include "dm_convert.h"
#include "netdev_rpc.h"

// 'empty' replacement for function in mod-dmext.so
static int _dummy_function(void) {
    return AMXB_STATUS_OK;
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

// read sig alarm is used to wait for a timer to run out so the callback function for this timer will be executed
void read_sig_alarm(void) {
    int rv = -1;
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;
    fd_set set;
    struct timeval timeout;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);

    FD_ZERO(&set);     /* clear the set */
    FD_SET(sfd, &set); /* add our file descriptor to the set */
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    rv = select(sfd + 1, &set, NULL, NULL, &timeout);
    if(rv == -1) {
        assert_non_null(NULL);
    } else if(rv == 0) {
        printf("No timer event occurred\n");
        fflush(stdout);
    } else {
        s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
        assert_int_equal(s, sizeof(struct signalfd_siginfo));
        if(fdsi.ssi_signo == SIGALRM) {
            amxp_timers_calculate();
            amxp_timers_check();
        }
    }
}

int validate_transaction(amxd_trans_t* trans, const char* select, amxd_action_t action_type, amxc_var_t* expected_data) {
    int rv = -1;
    when_str_empty_status(select, exit, rv = -2);
    amxc_llist_for_each(it, (&trans->actions)) {
        int result = -1;
        amxd_trans_action_t* action = amxc_llist_it_get_data(it, amxd_trans_action_t, it);
        if(action->action == action_any) {
            when_false_status(strcmp(select, GET_CHAR(&action->data, "path")) == 0, exit, rv = 1);
        } else {
            when_false_status(action->action == action_type, exit, rv = 2);
            when_failed_status(amxc_var_compare(&action->data, expected_data, &result), exit, rv = 3);
            when_failed_status(result, exit, rv = 4);
        }
    }
    rv = 0;
exit:
    return rv;
}

amxc_var_t* read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;

    // create a json reader
    if(amxj_reader_new(&reader) != 0) {
        printf("Failed to create json file reader");
        goto exit;
    }

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        printf("File open file %s - error 0x%8.8X\n", fname, errno);
        goto exit;
    }

    // read the json file and parse the json text
    while(amxj_read(reader, fd) > 0) {
    }

    // get the variant
    data = amxj_reader_result(reader);

    if(data == NULL) {
        printf("Invalid JSON in file %s\n", fname);
    }

    close(fd);
exit:
    amxj_reader_delete(&reader);
    return data;
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ip",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv4",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv6",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_macaddr",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "AddIPAddress",
                                            AMXO_FUNC(_AddIPAddress)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "UpdateIPAddress",
                                            AMXO_FUNC(_UpdateIPAddress)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "DeleteIPAddress",
                                            AMXO_FUNC(_DeleteIPAddress)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "GetDefaultRoute",
                                            AMXO_FUNC(_GetDefaultRoute)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "on_rtnltrace_changed",
                                            AMXO_FUNC(_on_rtnltrace_changed)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "addr_destroy",
                                            AMXO_FUNC(_addr_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "route_destroy",
                                            AMXO_FUNC(_route_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "neigh_destroy",
                                            AMXO_FUNC(_neigh_destroy)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_describe",
                                            AMXO_FUNC(_stats_describe)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_list",
                                            AMXO_FUNC(_stats_list)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_read",
                                            AMXO_FUNC(_stats_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_destroy",
                                            AMXO_FUNC(_stats_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "netlink_msgs_read",
                                            AMXO_FUNC(_netlink_msgs_read)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "table_destroy",
                                            AMXO_FUNC(_table_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "convert_entry_added",
                                            AMXO_FUNC(_convert_entry_added)), 0);

}
