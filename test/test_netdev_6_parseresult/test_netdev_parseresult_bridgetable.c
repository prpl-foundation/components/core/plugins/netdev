/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <libmnl/libmnl.h>

#include "test_netdev_common_setup.h"
#include "netdev_parseresult.h"
#include "test_netdev_parseresult_bridgetable.h"

int test_netdev_parseresult_bridgetable_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    return 0;
}

int test_netdev_parseresult_bridgetable_teardown(UNUSED void** state) {
    sahTraceClose();

    return 0;
}

typedef struct {
    const char* raw_header;
    const netdev_parseresult_bridgetable_t expected_parseresult;
} testdata_t;

static void s_assert_parseresult_eq(const netdev_parseresult_bridgetable_t* parseresult1, const netdev_parseresult_bridgetable_t* parseresult2) {
    assert_true((parseresult1 == NULL) == (parseresult2 == NULL));
    if(parseresult1 != NULL) {
        assert_true(parseresult1->disappeared == parseresult2->disappeared);
        assert_string_equal(parseresult1->mac, parseresult2->mac);
        assert_int_equal(parseresult1->netdev_index, parseresult2->netdev_index);
    }
}

static void s_testhelper_parse(const testdata_t* testdata) {
    assert_non_null(testdata);
    // GIVEN a netlink message
    const struct nlmsghdr* header = (const struct nlmsghdr*) testdata->raw_header;

    // WHEN parsing it
    netdev_parseresult_bridgetable_t* actual_parseresult = netdev_parseresult_bridgetable_parse(header);

    // THEN the parse result is as expected
    s_assert_parseresult_eq(actual_parseresult, &testdata->expected_parseresult);

    netdev_parseresult_bridgetable_delete(&actual_parseresult);
}

void test_netdev_parseresult_bridgetable_parse__unrelated(UNUSED void** state) {
    // GIVEN a message that is not a bridgetable message
    const char raw_header[] = "\x34\x00\x00\x00\x18\x00\x02\x00\x03\x00\x00\x00\x1c\x66\x00\x00"
        "\x02\x00\x00\x00\xfe\x03\x00\x01\x00\x00\x00\x00\x08\x00\x0f\x00"
        "\xfe\x00\x00\x00\x08\x00\x05\x00\xac\x11\x00\x01\x08\x00\x04\x00"
        "\x14\x00\x00\x00";
    const struct nlmsghdr* header = (const struct nlmsghdr*) raw_header;

    // WHEN parsing it
    printf("Expected to produce an ERROR trace\n");
    fflush(stdout);
    netdev_parseresult_bridgetable_t* parseresult = netdev_parseresult_bridgetable_parse(header);

    // THEN it is ignored
    assert_null(parseresult);
}

void test_netdev_parseresult_bridgetable_parse__appear(UNUSED void** state) {
    testdata_t testdata = {
        .raw_header = "\x44\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0b\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x58\xef\x68\xb4\x26\x2e\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x00\x00\x00\x00",
        .expected_parseresult = {
            .disappeared = false,
            .netdev_index = 11,
            .mac = "58:EF:68:B4:26:2E",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__disappear(UNUSED void** state) {
    testdata_t testdata = {
        // GIVEN a message that describes a bridgetable entry disappears
        .raw_header = "\x44\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0b\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x58\xef\x68\xb4\x26\x2e\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x63\x4c\x00\x00\xcd\x0f\x00\x00"
            "\x00\x00\x00\x00",

        // WHEN parsing it
        // THEN it is parsed as a disappearing bridgetable entry with correct data
        .expected_parseresult = {
            .disappeared = true,
            .netdev_index = 11,
            .mac = "58:EF:68:B4:26:2E",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__appear_while_other_port_also_in_use(UNUSED void** state) {
    testdata_t testdata = {
        .raw_header = "\x44\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0c\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x00\xe0\x4c\x36\x10\xe3\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x00\x00\x00\x00",
        .expected_parseresult = {
            .disappeared = false,
            .netdev_index = 12,
            .mac = "00:E0:4C:36:10:E3",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__disappears_while_other_port_stays_in_use(UNUSED void** state) {
    testdata_t testdata = {
        .raw_header = "\x44\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0b\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x58\xef\x68\xb4\x26\x2e\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x99\xd9\x00\x00\xc0\x00\x00\x00"
            "\x00\x00\x00\x00",
        .expected_parseresult = {
            .disappeared = true,
            .netdev_index = 11,
            .mac = "58:EF:68:B4:26:2E",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__appears_via_switch_inbetween(UNUSED void** state) {
    testdata_t testdata = {
        .raw_header = "\x44\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0c\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x58\xef\x68\xb4\x26\x2e\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x00\x00\x00\x00",
        .expected_parseresult = {
            .disappeared = false,
            .netdev_index = 12,
            .mac = "58:EF:68:B4:26:2E",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__appear_second_device_via_switch(UNUSED void** state) {
    testdata_t testdata = {
        // GIVEN a netlink message of a device appearing on a port for which another device is already
        //       present on that port (there's a switch between the port and these two devices)
        .raw_header = "\x44\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0c\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x00\xe0\x4c\x36\x10\xe3\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x00\x00\x00\x00",

        // WHEN parsing
        // THEN parse result is as expected
        .expected_parseresult = {
            .disappeared = false,
            .netdev_index = 12,
            .mac = "00:E0:4C:36:10:E3",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__disappear_through_switch_disconnect(UNUSED void** state) {
    testdata_t testdata = {
        // GIVEN a netlink message describing a device disappears. This happened due to disconnect
        //       of the switch inbetween the device and the port, where the switch had multiple devices
        //       connected
        .raw_header = "\x44\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0c\x00\x00\x00\x02\x00\x00\x00\x0a\x00\x02\x00"
            "\x00\xe0\x4c\x36\x10\xe3\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\xcc\x18\x00\x00\xad\x00\x00\x00"
            "\x00\x00\x00\x00",

        // WHEN parsing
        // THEN parse result is as expected
        .expected_parseresult = {
            .disappeared = true,
            .netdev_index = 12,
            .mac = "00:E0:4C:36:10:E3",
        }
    };
    s_testhelper_parse(&testdata);
}

void test_netdev_parseresult_bridgetable_parse__disappear_via_timeout(UNUSED void** state) {
    testdata_t testdata = {
        // GIVEN a netlink message received in the situation where a device disappears
        //       from the bridgetable due to timeout (not due to cable disconnect because
        //       there was a switch inbetween the device and the port where the switch cable
        //       stayed connected)
        .raw_header = "\x44\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
            "\x07\x00\x00\x00\x0c\x00\x00\x00\x04\x00\x00\x00\x0a\x00\x02\x00"
            "\x00\xe0\x4c\x36\x10\xe3\x00\x00\x08\x00\x09\x00\x12\x00\x00\x00"
            "\x14\x00\x03\x00\x00\x00\x00\x00\xcc\xf4\x00\x00\x99\x79\x00\x00"
            "\x00\x00\x00\x00",

        // WHEN parsing the message
        // THEN the parse result is as expected
        .expected_parseresult = {
            .disappeared = true,
            .netdev_index = 12,
            .mac = "00:E0:4C:36:10:E3",
        }
    };
    s_testhelper_parse(&testdata);
}
