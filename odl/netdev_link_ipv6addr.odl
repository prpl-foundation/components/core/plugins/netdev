%define {
    select NetDev.Link {
        /**
         *
         * @version 1.0
         */
        object IPv6Addr[] {
            on action destroy call addr_destroy;

            /**
             * The number of entries.
             * @version 1.0
             */
            counted with IPv6AddrNumberOfEntries;

            /**
            * A non-volatile unique key used to reference this instance. 
            * Alias provides a mechanism for a Controller to label this instance for future reference. 
            *
            * @version 1.0
            */
            %unique %key string Alias;

            /**
             * IPv6 Address as a string
             * @version 1.0
             */
            %read-only string Address {
                on action validate call is_valid_ipv6;
            }

            /**
             * IPv6 Address of the peer, if relevant.
             * @version 1.0
             */
            %read-only string Peer {
                on action validate call is_valid_ipv6;
            }

            /**
             * Number of bits in the prefix
             * Expressed as an integer between 1 through 128.
             * @version 1.0
             */
            %read-only uint8 PrefixLen;

            /**
             * Address flags.
             * Represented as a space seperated list of keywords.
             * Some possible values include:
             * secondary nodad optimistic homeaddress deprecated tentative permanent dadfailed
             * @version 1.0
             */
            %read-only string Flags;

            /**
             * Address scopr.
             * Some possible values:
             * global site link host nowhere | user defined from NetDev.ConversionTable.Scope
             * @version 1.0
             */
            %read-only string Scope = "global";

            /** 
             * Define the type of address, each address type starts with an '@' sign.
             * The following types are supported for ipv6: @gua, @lla, @ula, @mc.
             * @version 1.0
             */
            string TypeFlags;

            /**
             * Expressed in seconds since boot, this is probably in the future ( > Uptime), or 0 if preferred forever
             * @version 1.0
             */
            %read-only uint32 PreferredLifetime;

            /**
             * Expressed in seconds since boot, this is definitely in the future ( > Uptime), or 0 if valid forever
             * @version 1.0
             */
            %read-only uint32 ValidLifetime;

            /**
             * Expressed in seconds since boot, this is definitely in the past ( < Uptime)
             * @version 1.0
             */
            %read-only uint32 CreatedTimestamp;

            /**
             * Expressed in seconds since boot, this is definitely in the past ( < Uptime)
             * @version 1.0
             */
            %read-only uint32 UpdatedTimestamp;
        }
    }
}
