/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_LINK_ADDR_H__)
#define __DM_LINK_ADDR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "dm_netdev.h"
#include "netdev_util.h"
#include "dm_convert.h"

typedef struct {
    amxc_llist_it_t it;
    int link;
    unsigned char family;
    unsigned char prefixlen;
    unsigned char flags;
    unsigned char scope;
    unsigned char address[ADDRSPACE];
    unsigned char peer[ADDRSPACE];
    unsigned long preferred_lifetime;
    unsigned long valid_lifetime;
    unsigned long created_timestamp;
    unsigned long updated_timestamp;
    unsigned int dyn_index;
    unsigned int index;
    amxd_object_t* object;
    amxp_timer_t* mercytimer;
    bool active; // Do not set to false unless deferred_destroy is scheduled
} addr_t;

converter_t* init_addr_flags_converter(void);
converter_t* init_addr_scope_converter(void);
void addr_init_converters(void);

addr_t* addr_create(void);
void addr_destroy(addr_t* addr, bool deleteObject);
addr_t* addr_find(addr_t* ref);
int addr_copy(addr_t* dst, addr_t* src);
int msg2addr(addr_t* addr, rtnlmsg_t* msg);
amxd_trans_t* create_transaction_from_addr(addr_t* addr, bool new_inst);
int handleNewAddr(rtnlmsg_t* msg);
int handleDelAddr(rtnlmsg_t* msg);

amxd_status_t _addr_destroy(amxd_object_t* object,
                            amxd_param_t* param,
                            amxd_action_t reason,
                            const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            void* priv);

bool addr_init(void);
bool addr_start(void);
void addr_cleanup(void);

#ifdef __cplusplus
}
#endif

#endif // __DM_LINK_ADDR_H__
