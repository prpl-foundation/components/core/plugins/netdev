/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_LINK_H__)
#define __DM_LINK_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <linux/rtnetlink.h>

#include "netdev.h"
#include "netdev_util.h"
#include "netdev_netlink.h"
#include "dm_convert.h"

#ifndef IFF_NOMULTIPATH
#define IFF_NOMULTIPATH 0x80000     /* Disable for MPTCP        */
#endif

typedef struct {
    amxc_llist_it_t it;
    const char* kind;
    void (* print)(FILE* f, struct rtattr* data);
} link_info_t;

typedef struct {
    amxc_llist_it_t it;
    int index;
    unsigned short type;
    unsigned int flags;
    char name[LINKNAMESPACE];
    unsigned int master;
    lladdr_t lladdr;
    unsigned int txqueuelen;
    unsigned int mtu;
    unsigned char state;
    amxd_object_t* object;
    uint32_t seqthreshold;
    bool active; // Do not set to false unless deferred_destroy is scheduled
} link_t;

void new_link_deferred_transaction(const amxc_var_t* data, void* priv);

link_t* link_create(int index);
void link_destroy(link_t* link, bool deleteObject);
link_t* link_find(int index);

int update_link_info_from_msg(link_t* link, rtnlmsg_t* msg);
amxd_trans_t* create_transaction_from_link(link_t* link, bool new_inst);
int add_bridge_info_to_transaction(link_t* link, rtnlmsg_t* msg, amxd_trans_t* trans, bool new_inst);
int handleNewLink(rtnlmsg_t* msg);
int handleDelLink(rtnlmsg_t* msg);

converter_t* init_link_type_converter(void);
converter_t* init_link_flags_converter(void);
converter_t* init_link_state_converter(void);
converter_t* init_link_bridge_stpstate_converter(void);
void link_init_converters(void);

bool dm_link_init(void);
void dm_link_cleanup(void);

bool dm_link_start(void);

void link_register_info(link_info_t* info);
void link_unregister_info(link_info_t* info);

bool read_link(amxd_object_t* object);
bool read_stats(amxd_object_t* object);

#ifdef __cplusplus
}
#endif

#endif // __DM_LINK_H__
