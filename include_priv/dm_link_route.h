/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DM_LINK_ROUTE_H__)
#define __DM_LINK_ROUTE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "netdev_util.h"
#include "dm_convert.h"
#include "dm_link_neigh.h"
#include "netdev_bridgetable.h"

typedef struct _route {
    amxc_llist_it_t it;
    // key start
    unsigned char family;
    unsigned char dstlen;
    unsigned char table;
    unsigned char protocol;
    unsigned char scope;
    unsigned char type;
    unsigned char dst[ADDRSPACE];
    int priority;
    int oif;
    // key end
    unsigned char gateway[ADDRSPACE];
    unsigned char prefsrc[ADDRSPACE];
    unsigned int mtu;
    unsigned int advmss;
    unsigned int hoplimit;
    unsigned int dyn_index;
    unsigned int index;
    amxd_object_t* object;
    /* Delete routes with mercy:
       if the same route is re-created right after it was deleted,
       in NetDev you won't notice. This happens on the target for the default route
     */
    amxp_timer_t* mercytimer;
    bool active;                          // Do not set to false unless deferred_destroy is scheduled
    bool default_route;                   // Define whether a route is default or not. Creates the link with the corresponding neighbour structure
    struct _neigh* def_neigh;             // Reference to the neighbour linked to the default route
    struct _bridgetable* def_bridgetable; // Reference to the bridge table linked to the default route
} route_t;

converter_t* init_route_table_converter(void);
converter_t* init_route_protocol_converter(void);
converter_t* init_route_scope_converter(void);
converter_t* init_route_type_converter(void);

amxd_status_t _route_destroy(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

route_t* route_lookup_ipv4_default_route(void);
amxd_status_t route_convert_to_default_route_var(route_t* defroute, bool active, amxc_var_t* data);
bool set_if_default_route(route_t* route);
amxd_status_t default_route_event(route_t* route, bool active);

route_t* route_find(route_t* ref);
void route_destroy(route_t* route, bool deleteObject);
route_t* route_create(void);
int route_copy(route_t* dst, route_t* src);
int msg2route(route_t* route, rtnlmsg_t* msg);
route_t* neigh_bind_to_defroute(struct _neigh* neigh, bool ipv4);
bool neigh_defroute_match(struct _neigh* neigh, route_t* defroute, bool ipv4);
route_t* bridgetable_bind_to_defroute(bridgetable_t* bridgetable, bool ipv4);
bool bridgetable_defroute_match(bridgetable_t* bridgetable, route_t* defroute, bool ipv4);

amxd_trans_t* create_transaction_from_route(route_t* route, bool new_inst);
int handleNewRoute(rtnlmsg_t* msg);
int handleDelRoute(rtnlmsg_t* msg);

void route_init_converters(void);

bool route_init(void);
bool route_start(void);
void route_cleanup(void);
void route_clear(void);

void route_deleteIPv4Routes(int link);

#ifdef __cplusplus
}
#endif

#endif // __DM_LINK_ROUTE_H__

